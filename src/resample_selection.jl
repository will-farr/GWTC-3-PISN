"""
    resample_selection(wt_fn, m1s_sel, qs_sel, zs_sel, pdraw_sel, Ndraw_sel)

Return (m1s, qs, zs, pdraws, Ndraw, Neff_norm), a resampling of the selection
function Monte Carlo according to the given weight function.

The first five return values correspond to the input selection function Monte
Carlo samples, but as if the injections had been drawn from the given weight
function; the final value, `Neff_norm` represents an additional error term in
the normalization of `pdraw` for the new weights due to the finite number of
samples.  Using the terminology of [Farr
(2019)](http://doi.org/10.3847/2515-5172/ab1d5f), the final uncertainty in the
selection function estimate using the resampled parameters is given by

``N_\\mathrm{eff}^{-1} = N_\\mathrm{eff,est}^{-1} + N_\\mathrm{eff,norm}^{-1}``

where ``N_\\mathrm{eff,est}`` is the estimate of the Monte Carlo uncertainty
from the resampled injection set as described in that reference.
"""
function resample_selection(wt_fn, m1s_sel, qs_sel, zs_sel, pdraw_sel, Ndraw_sel)
    wts = [wt_fn(m,q,z) for (m,q,z) in zip(m1s_sel, qs_sel, zs_sel)] ./ pdraw_sel
    norm = sum(wts) / Ndraw_sel
    s2 = sum(wts.*wts) / (Ndraw_sel*Ndraw_sel) - norm*norm/Ndraw_sel
    Neff_norm = norm*norm / s2
    neff = sum(wts)^2 / sum(wts.*wts)

    inds = sample(1:length(m1s_sel), StatsBase.weights(wts), round(Int, neff), replace=true)

    m1s = m1s_sel[inds]
    qs = qs_sel[inds]
    zs = zs_sel[inds]
    pdraw = [wt_fn(m,q,z) / norm for (m,q,z) in zip(m1s, qs, zs)]
    Ndraw = length(m1s)

    (m1s, qs, zs, pdraw, Ndraw, Neff_norm)
end
