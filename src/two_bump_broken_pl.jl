function _log_bpl(x, a1, a2, xb)
    y = (x-xb)/(0.02*xb)

    # This was a long-standing bug: logistic --> 1 when its argument goes to
    # infty.  We want to activate the "1" power law when x << xb, hence the
    # signs below that seem, at first blush, to be "backwards."
    log_f1 = log_logistic_unit(-y)
    log_f2 = log_logistic_unit(y)

    z = x/xb

    logaddexp(log_f1 + a1*log(z),
              log_f2 + a2*log(z))
end

"""
    log_dNdm1dqdz_tbbpl_fn(a1, a2, b, mb, f1, mu1, sig1, f2, mu2, sig2, lam)

Returns a function representing a population model that is a sum of a broken
power law and two Gaussian bumps.

The primary mass distribution is a sum of three components: a power law

``\\frac{\\mathrm{d} N}{\\mathrm{d} m_1} = \\begin{cases}
\\left( \\frac{m_1}{m_b} \\right)^{-\\alpha_1} & m_1 < m_b \\\\
\\left( \\frac{m_1}{m_b} \\right)^{-\\alpha_2} & m_1 \\geq m_b
\\end{cases}``

And two Gaussian components

``\\frac{\\mathrm{d} N}{\\mathrm{d} m_1} = \\exp\\left( - \\frac{\\left( m_1 - \\mu_{1,2} \\right)^2}{2 \\sigma_{1,2}^2} \\right)``

whose amplitudes are `f1` and `f2` times the power law.

The redshift evolution follows a power law

``\\frac{\\mathrm{d} N}{\\mathrm{d} V \\mathrm{d} t} = \\left( 1 + z \\right)^{\\lambda}``

# Arguments

- `a1` The negative power law slope at low primary masses.
- `a2` The negative power law slope at high primary masses.
- `b` The mass ratio power law slope.
- `mb` The "break" mass where the primary mass power law transitions from `-a1` to `-a2`.
- `f1` The amplitude of the first Gaussian (relative to the power law evaluated at the peak mass).
- `mu1` The peak location of the first Gaussian.
- `sig1` The width (s.d.) of the first Gaussian.
- `f2` The amplitude of the second Gaussian (relative to the power law at the peak).
- `mu2` The peak of the second Gaussian.
- `sig2` The width (s.d.) of the second Gaussian.
- `lam` The low-redshift power law.
"""
function log_dNdm1dqdz_tbbpl_fn(a1, a2, b, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap; mmin=5, mmax=200, m_norm=35.0, q_norm=0.8, z_norm=0.4)
    log_amp1 = log(f1) + _log_bpl(mu1, -a1, -a2, mb)
    log_amp2 = log(f2) + _log_bpl(mu2, -a1, -a2, mb)

    function log_dNdm1dqdz_unnorm(m1, q, z)
        if m1 > mmax || q*m1 < mmin
            -Inf
        else
            log_dNdm_pl = _log_bpl(m1, -a1, -a2, mb)
            log_dNdm_g1 = log_amp1 - 0.5*(m1-mu1)*(m1-mu1)/(sig1*sig1)
            log_dNdm_g2 = log_amp2 - 0.5*(m1-mu2)*(m1-mu2)/(sig2*sig2)

            log_dNdm = logsumexp([log_dNdm_pl, log_dNdm_g1, log_dNdm_g2])
            log_dNdq = b*log(q)
            log_dNdVdt = lam*log1p(z) - log1p(((1 + z) / (1 + zp))^kap)
            log_dNdz = log_dNdVdt + log(dVdz_default(z)) - log1p(z)

            log_dNdm + log_dNdq + log_dNdz
        end
    end

    log_norm = log_dNdm1dqdz_unnorm(m_norm, q_norm, z_norm) + log(m_norm) - log(dVdz_default(z_norm)) + log1p(z_norm)

    (m1, q, z) -> log_dNdm1dqdz_unnorm(m1, q, z) - log_norm
end

"""
    log_dNdm1dqdz_tbbpl_paired_fn(a1, a2, b, mb, f1, mu1, sig1, f2, mu2, sig2, lam)

Returns a function representing a population model that is a sum of a broken
power law and two Gaussian bumps.

Both mass distributions are a sum of three components: a power law

``\\frac{\\mathrm{d} N}{\\mathrm{d} m_1} = \\begin{cases}
\\left( \\frac{m_1}{m_b} \\right)^{-\\alpha_1} & m_1 < m_b \\\\
\\left( \\frac{m_1}{m_b} \\right)^{-\\alpha_2} & m_1 \\geq m_b
\\end{cases}``

And two Gaussian components

``\\frac{\\mathrm{d} N}{\\mathrm{d} m_1} = \\exp\\left( - \\frac{\\left( m_1 - \\mu_{1,2} \\right)^2}{2 \\sigma_{1,2}^2} \\right)``

whose amplitudes are `f1` and `f2` times the power law.

The redshift evolution follows a power law

``\\frac{\\mathrm{d} N}{\\mathrm{d} V \\mathrm{d} t} = \\left( 1 + z \\right)^{\\lambda}``

There is a power-law "pairing function" in `q`, proportional to ``q^\\beta``.

# Arguments

- `a1` The negative power law slope at low primary masses.
- `a2` The negative power law slope at high primary masses.
- `b` The pairing function mass ratio power law slope.
- `mb` The "break" mass where the primary mass power law transitions from `-a1` to `-a2`.
- `f1` The amplitude of the first Gaussian (relative to the power law evaluated at the peak mass).
- `mu1` The peak location of the first Gaussian.
- `sig1` The width (s.d.) of the first Gaussian.
- `f2` The amplitude of the second Gaussian (relative to the power law at the peak).
- `mu2` The peak of the second Gaussian.
- `sig2` The width (s.d.) of the second Gaussian.
- `lam` The low-redshift power law.
"""
function log_dNdm1dqdz_tbbpl_paired_fn(a1, a2, b, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap; mmin=5, mmax=200, m_norm=35.0, q_norm=0.8, z_norm=0.4)
    log_amp1 = log(f1) + _log_bpl(mu1, -a1, -a2, mb)
    log_amp2 = log(f2) + _log_bpl(mu2, -a1, -a2, mb)

    function log_dNdm1dqdz_unnorm(m1, q, z)
        m2 = q*m1
        if m1 > mmax || m2 < mmin
            -Inf
        else
            log_dNdm1_pl = _log_bpl(m1, -a1, -a2, mb)
            log_dNdm1_g1 = log_amp1 - 0.5*(m1-mu1)*(m1-mu1)/(sig1*sig1)
            log_dNdm1_g2 = log_amp2 - 0.5*(m1-mu2)*(m1-mu2)/(sig2*sig2)

            log_dNdm2_pl = _log_bpl(m2, -a1, -a2, mb)
            log_dNdm2_g1 = log_amp1 - 0.5*(m2-mu1)*(m2-mu1)/(sig1*sig1)
            log_dNdm2_g2 = log_amp2 - 0.5*(m2-mu2)*(m2-mu2)/(sig2*sig2)

            log_dNdm1 = logsumexp([log_dNdm1_pl, log_dNdm1_g1, log_dNdm1_g2])
            log_dNdm2 = logsumexp([log_dNdm2_pl, log_dNdm2_g1, log_dNdm2_g2])
            log_pf = b*log(q)
            log_dNdVdt = lam*log1p(z) - log1p(((1 + z) / (1 + zp))^kap)
            log_dNdz = log_dNdVdt + log(dVdz_default(z)) - log1p(z)

            log_dNdm1 + (log_dNdm2 + log(m1)) + log_pf + log_dNdz
        end
    end

    log_norm = log_dNdm1dqdz_unnorm(m_norm, q_norm, z_norm) + log(m_norm) - log(dVdz_default(z_norm)) + log1p(z_norm)

    (m1, q, z) -> log_dNdm1dqdz_unnorm(m1, q, z) - log_norm
end

function log_dNdm1dqdz_tbbpl_paired_fn(trace::Chains; mmin=5, mmax=200, m_norm=35.0, q_norm=0.8, z_norm=0.4)
    map((x,y...) -> log_dNdm1dqdz_tbbpl_paired_fn(x, y...; mmin=mmin, mmax=mmax, m_norm=m_norm, q_norm=q_norm, z_norm=z_norm),
        trace[:alpha1], trace[:alpha2], trace[:beta], trace[:mb], trace[:f1], trace[:mu1], trace[:sig1], trace[:f2], trace[:mu2], trace[:sig2], trace[:lam], trace[:zp], trace[:kap])
end

"""
    two_bump_broken_pl_model(m1s, qs, zs, log_wt, m1s_sel, qs_sel, zs_sel, log_pdraw_sel, Ndraw; Neff_norm=Inf, paired=false)

Turing model for fitting the two-bump broken power law distribution.

# Arguments

- `m1s` Array of size `(Nsamp, Nevt)` of `Nsamp` samples of `m1` from each of
  the `Nevt` posteriors.
- `qs` Samples of the mass ratio.
- `zs` Samples of the redshift.
- `log_wt` The natural log of the prior weight over `(m1,q,z)` for each sample.
- `m1s_sel` Samples of `m1` for detected injections.
- `qs_sel` Samples of `q` from detected injections.
- `zs_sel` Samples of `z` from detected injections.
- `log_pdraw_sel` The natural log of the density over `(m1, q, z, t_detector)`
  from which each detected injection has been drawn.
- `Ndraw` The total number of injection draws generating the set of detected
  injections.
- `Neff_norm=Inf` Optional parameter giving the accuracy to which the
  normalization of `log_pdraw_sel` is determined (used when re-weighting
  injections for efficiency before fitting, as described in
  https://dcc.ligo.org/LIGO-T2100068)
- `paired=false` Whether to use the "paired" mass distribution, where both
  components get the two-bump broken power law structure.
"""
@model function two_bump_broken_pl_model(m1s, qs, zs, log_wt, m1s_sel, qs_sel, zs_sel, log_pdraw_sel, Ndraw; Neff_norm=Inf, paired=false)
    nevt, nsamp = size(m1s)

    alpha1 ~ Uniform(0.0, 8.0)
    alpha2 ~ Uniform(0, 8.0)
    beta ~ Uniform(-2, 8)
    mb ~ Uniform(25, 60)

    log_f1 ~ Uniform(log(0.1), log(20.0))
    log_f2 ~ Uniform(log(0.1), log(10.0))

    f1 = exp(log_f1)
    f2 = exp(log_f2)

    mu1 ~ Uniform(5, 20)
    mu2 ~ Uniform(20, 60)

    log_sig1 ~ Uniform(log(0.5), log(15.0))
    log_sig2 ~ Uniform(log(1.0), log(40.0))
    sig1 = exp(log_sig1)
    sig2 = exp(log_sig2)

    lam ~ Normal(2.7, 2.0)
    dkap ~ truncated(Normal(5.6-2.7, 2.0), 1, Inf)
    kap = lam + dkap
    zp ~ truncated(Normal(1.9, 1.0), 0, Inf)

    if paired
        log_dNdm1dqdz = log_dNdm1dqdz_tbbpl_paired_fn(alpha1, alpha2, beta, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap)
    else
        log_dNdm1dqdz = log_dNdm1dqdz_tbbpl_fn(alpha1, alpha2, beta, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap)
    end

    log_dN = log_dNdm1dqdz.(m1s, qs, zs) .- log_wt

    Turing.@addlogprob! sum(logsumexp(log_dN, dims=2) .- log(nsamp))

    Neff_samps = [exp(logsumexp(log_dN[i,:]) - maximum(log_dN[i,:])) for i in 1:nevt]

    log_dN_sel = log_dNdm1dqdz.(m1s_sel, qs_sel, zs_sel)
    log_mu, Neff_raw = log_mu_selection(log_dN_sel, log_pdraw_sel, Ndraw)
    Neff = 1 / (1/Neff_raw + 1/Neff_norm)
    Turing.@addlogprob! -nevt*log_mu

    mu = exp(log_mu)
    mu_R = nevt / mu
    sigma_R = sqrt(nevt) / mu

    R = rand(Normal(mu_R, sigma_R))

    return (f1=f1, f2=f2, sig1=sig1, sig2=sig2, R=R, Neff=Neff, kap=kap, Neff_samps=Neff_samps)
end

"""
    two_bump_broken_pl_cosmology_model(m1ds, qs, dls, log_wt, m1ds_sel, qs_sel, dls_sel, log_pdraw_sel, Ndraw; Neff_norm=Inf, paired=true)

Turing model for fitting the two-bump broken power law distribution.

# Arguments

- `m1ds` Array of size `(Nsamp, Nevt)` of `Nsamp` samples of `m1_det` from each
  of the `Nevt` posteriors.
- `qs` Samples of the mass ratio.
- `dls` Samples of the luminosity distance (Gpc).
- `log_wt` The natural log of the prior weight over `(m1d,q,dl)` for each sample.
- `m1ds_sel` Samples of `m1_det` for detected injections.
- `qs_sel` Samples of `q` from detected injections.
- `dls_sel` Samples of `z` from detected injections.
- `log_pdraw_sel` The natural log of the density over `(m1d, q, dl, t_detector)`
  from which each detected injection has been drawn.
- `Ndraw` The total number of injection draws generating the set of detected
  injections.
- `Neff_norm=Inf` Optional parameter giving the accuracy to which the
  normalization of `log_pdraw_sel` is determined (used when re-weighting
  injections for efficiency before fitting, as described in
  https://dcc.ligo.org/LIGO-T2100068)
- `paired=true` Whether to use the "paired" mass distribution, where both
  components get the two-bump broken power law structure.
"""
@model function two_bump_broken_pl_cosmology_model(m1ds, qs, dls, log_wt, m1ds_sel, qs_sel, dls_sel, log_pdraw_sel, Ndraw; Neff_norm=Inf, paired=true, zmax=20)
    nevt, nsamp = size(m1ds)
    nsel = length(m1ds_sel)

    h ~ truncated(Normal(0.7, 0.2), 0.35, 1.4)
    ΩM ~ truncated(Normal(0.3, 0.1), 0, 1)
    w ~ truncated(Normal(-1, 0.25), -1.5, -0.5)

    alpha1 ~ Uniform(0.0, 8.0)
    alpha2 ~ Uniform(0, 8.0)
    beta ~ Uniform(-2, 8)
    mb ~ Uniform(25, 60)

    log_f1 ~ Uniform(log(0.1), log(20.0))
    log_f2 ~ Uniform(log(0.1), log(10.0))

    f1 = exp(log_f1)
    f2 = exp(log_f2)

    mu1 ~ Uniform(5, 20)
    mu2 ~ Uniform(20, 60)

    log_sig1 ~ Uniform(log(0.5), log(15.0))
    log_sig2 ~ Uniform(log(1.0), log(40.0))
    sig1 = exp(log_sig1)
    sig2 = exp(log_sig2)

    lam ~ Normal(2.7, 2.0)
    dkap ~ truncated(Normal(5.6-2.7, 2.0), 1, Inf)
    kap = lam + dkap
    zp ~ truncated(Normal(1.9, 1.0), 0, Inf)

    dH_ = dH(h)
    zs = expm1.(log(1.0):0.01:log(1.0+zmax))
    dC = dCs(zs, ΩM, w)
    dL = dLs(zs, dC)
    ddLdz = ddLsdzs(zs, dL, ΩM, w)
    dVdz = (4*pi) .* PISNMassFunctions.comoving_volume_element(zs, dC, ΩM, w)

    dC = dC .* dH_
    dL = dL .* dH_
    ddLdz = ddLdz .* dH_
    dVdz = dVdz .* (dH_*dH_*dH_)

    z_of_dl = interp(dL, zs)
    dV_of_z = interp(zs, dVdz)
    ddL_of_z = interp(zs, ddLdz)

    if paired
        log_dNdm1dqdz = log_dNdm1dqdz_tbbpl_paired_fn(alpha1, alpha2, beta, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap)
    else
        log_dNdm1dqdz = log_dNdm1dqdz_tbbpl_fn(alpha1, alpha2, beta, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap)
    end

    zs = z_of_dl.(dls)
    zs_sel = z_of_dl.(dls_sel)

    m1s = m1ds ./ (1 .+ zs)
    m1s_sel = m1ds_sel ./ (1 .+ zs_sel)

    log_dN = log_dNdm1dqdz.(m1s, qs, zs) .+ log.(dV_of_z.(zs)) .- log.(dVdz_default.(zs)) .- log1p.(zs) .- log.(ddL_of_z.(zs)) .- log_wt

    log_dN_sum = logsumexp(log_dN, dims=2)
    Turing.@addlogprob! sum(log_dN_sum .- log(nsamp))

    Neff_samps = [exp(2*logsumexp(log_dN[i,:]) - logsumexp(2 .* log_dN[i,:])) for i in 1:nevt]

    log_dN_sel = log_dNdm1dqdz.(m1s_sel, qs_sel, zs_sel) .+ log.(dV_of_z.(zs_sel)) .- log.(dVdz_default.(zs_sel)) .- log1p.(zs_sel) .- log.(ddL_of_z.(zs_sel))
    log_mu, Neff_raw = log_mu_selection(log_dN_sel, log_pdraw_sel, Ndraw)
    Neff = 1 / (1/Neff_raw + 1/Neff_norm)
    Turing.@addlogprob! -nevt*log_mu

    mu = exp(log_mu)
    mu_R = nevt / mu
    sigma_R = sqrt(nevt) / mu

    R = rand(Normal(mu_R, sigma_R))

    # Draw random sample
    inds = [sample(1:nsamp, weights(exp.(log_dN[i,:] .- log_dN_sum[i]))) for i in 1:nevt]
    m1det = [m1ds[i,inds[i]] for i in 1:nevt]
    m1 = [m1s[i,inds[i]] for i in 1:nevt]
    q = [qs[i,inds[i]] for i in 1:nevt]
    z = [zs[i,inds[i]] for i in 1:nevt]
    dl = [dls[i,inds[i]] for i in 1:nevt]

    log_wt_sel = log_dN_sel .- log_pdraw_sel
    log_wt_sel = log_wt_sel .- logsumexp(log_wt_sel)
    inds = sample(1:nsel, weights(exp.(log_wt_sel)), nevt)

    return (f1=f1, f2=f2, sig1=sig1, sig2=sig2, R=R, Neff=Neff, kap=kap, Neff_samps=Neff_samps,
            m1dets=m1det, m1s=m1, qs=q, zs=z, dls=dl,
            m1dets_draw=m1ds_sel[inds], m1s_draw=m1s_sel[inds], qs_draw=qs_sel[inds], zs_draw=zs_sel[inds], dls_draw=dls_sel[inds])
end

"""
    two_bump_broken_pl_extra_dims(m1ds, qs, dls, log_wt, m1ds_sel, qs_sel, dls_sel, log_pdraw_sel, Ndraw; Neff_norm=Inf, paired=true)

Turing model for fitting the two-bump broken power law distribution and modified
luminosity distance.

We assume that the gravitational wave luminosity distance is related to the LCDM
luminosity distance via an exponential term; this encompasses at linear order
extra dimensions as well as graviton decay, etc.

```math
d_L = d_{L,\\mathrm{GW}} \\exp\\left( A_d d_{L,\\mathrm{GW}} \\right)
```

# Arguments

- `m1ds` Array of size `(Nsamp, Nevt)` of `Nsamp` samples of `m1_det` from each
  of the `Nevt` posteriors.
- `qs` Samples of the mass ratio.
- `dls` Samples of the luminosity distance (Gpc).
- `log_wt` The natural log of the prior weight over `(m1d,q,dl)` for each sample.
- `m1ds_sel` Samples of `m1_det` for detected injections.
- `qs_sel` Samples of `q` from detected injections.
- `dls_sel` Samples of `z` from detected injections.
- `log_pdraw_sel` The natural log of the density over `(m1d, q, dl, t_detector)`
  from which each detected injection has been drawn.
- `Ndraw` The total number of injection draws generating the set of detected
  injections.
- `Neff_norm=Inf` Optional parameter giving the accuracy to which the
  normalization of `log_pdraw_sel` is determined (used when re-weighting
  injections for efficiency before fitting, as described in
  https://dcc.ligo.org/LIGO-T2100068)
- `paired=true` Whether to use the "paired" mass distribution, where both
  components get the two-bump broken power law structure.
"""
@model function two_bump_broken_pl_extra_dims(m1ds, qs, dls, log_wt, m1ds_sel, qs_sel, dls_sel, log_pdraw_sel, Ndraw; Neff_norm=Inf, paired=true, zmax=20, A_d_prior_range=0.5)
    nevt, nsamp = size(m1ds)
    nsel = length(m1ds_sel)

    h = h_default
    ΩM = ΩM_default
    w = w_default

    alpha1 ~ Uniform(0.0, 8.0)
    alpha2 ~ Uniform(0, 8.0)
    beta ~ Uniform(-2, 8)
    mb ~ Uniform(25, 60)

    log_f1 ~ Uniform(log(0.1), log(20.0))
    log_f2 ~ Uniform(log(0.1), log(10.0))

    f1 = exp(log_f1)
    f2 = exp(log_f2)

    mu1 ~ Uniform(5, 20)
    mu2 ~ Uniform(20, 60)

    log_sig1 ~ Uniform(log(0.5), log(15.0))
    log_sig2 ~ Uniform(log(1.0), log(40.0))
    sig1 = exp(log_sig1)
    sig2 = exp(log_sig2)

    lam ~ Normal(2.7, 2.0)
    dkap ~ truncated(Normal(5.6-2.7, 2.0), 1, Inf)
    kap = lam + dkap
    zp ~ truncated(Normal(1.9, 1.0), 0, Inf)

    A_d ~ Uniform(-A_d_prior_range, A_d_prior_range)

    dH_ = dH(h)
    zs = expm1.(log(1.0):0.01:log(1.0+zmax))
    dC = dCs(zs, ΩM, w)
    dL = dLs(zs, dC)
    ddLdz = ddLsdzs(zs, dL, ΩM, w)
    dVdz = (4*pi) .* PISNMassFunctions.comoving_volume_element(zs, dC, ΩM, w)

    dC = dC .* dH_
    dL = dL .* dH_
    ddLdz = ddLdz .* dH_
    dVdz = dVdz .* (dH_*dH_*dH_)

    z_of_dl = interp(dL, zs)
    dV_of_z = interp(zs, dVdz)
    ddL_of_z = interp(zs, ddLdz)

    if paired
        log_dNdm1dqdz = log_dNdm1dqdz_tbbpl_paired_fn(alpha1, alpha2, beta, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap)
    else
        log_dNdm1dqdz = log_dNdm1dqdz_tbbpl_fn(alpha1, alpha2, beta, mb, f1, mu1, sig1, f2, mu2, sig2, lam, zp, kap)
    end

    dls_cosmo = dls .* exp.(A_d .* dls)
    dls_cosmo_sel = dls_sel .* exp.(A_d .* dls_sel)

    zs = z_of_dl.(dls_cosmo)
    zs_sel = z_of_dl.(dls_cosmo_sel)

    m1s = m1ds ./ (1 .+ zs)
    m1s_sel = m1ds_sel ./ (1 .+ zs_sel)

    log_dN = log_dNdm1dqdz.(m1s, qs, zs) .+ log.(dV_of_z.(zs)) .- log.(dVdz_default.(zs)) .- log1p.(zs) .- log.(ddL_of_z.(zs)) .- log_wt

    log_dN_sum = logsumexp(log_dN, dims=2)
    Turing.@addlogprob! sum(log_dN_sum .- log(nsamp))

    Neff_samps = [exp(2*logsumexp(log_dN[i,:]) - logsumexp(2 .* log_dN[i,:])) for i in 1:nevt]

    log_dN_sel = log_dNdm1dqdz.(m1s_sel, qs_sel, zs_sel) .+ log.(dV_of_z.(zs_sel)) .- log.(dVdz_default.(zs_sel)) .- log1p.(zs_sel) .- log.(ddL_of_z.(zs_sel))
    log_mu, Neff_raw = log_mu_selection(log_dN_sel, log_pdraw_sel, Ndraw)
    Neff = 1 / (1/Neff_raw + 1/Neff_norm)
    Turing.@addlogprob! -nevt*log_mu

    mu = exp(log_mu)
    mu_R = nevt / mu
    sigma_R = sqrt(nevt) / mu

    R = rand(Normal(mu_R, sigma_R))

    # Draw random sample
    inds = [sample(1:nsamp, weights(exp.(log_dN[i,:] .- log_dN_sum[i]))) for i in 1:nevt]
    m1det = [m1ds[i,inds[i]] for i in 1:nevt]
    m1 = [m1s[i,inds[i]] for i in 1:nevt]
    q = [qs[i,inds[i]] for i in 1:nevt]
    z = [zs[i,inds[i]] for i in 1:nevt]
    dl = [dls[i,inds[i]] for i in 1:nevt]

    log_wt_sel = log_dN_sel .- log_pdraw_sel
    log_wt_sel = log_wt_sel .- logsumexp(log_wt_sel)
    inds = sample(1:nsel, weights(exp.(log_wt_sel)), nevt)

    return (f1=f1, f2=f2, sig1=sig1, sig2=sig2, R=R, Neff=Neff, kap=kap, Neff_samps=Neff_samps,
            m1dets=m1det, m1s=m1, qs=q, zs=z, dls=dl,
            m1dets_draw=m1ds_sel[inds], m1s_draw=m1s_sel[inds], qs_draw=qs_sel[inds], zs_draw=zs_sel[inds], dls_draw=dls_sel[inds])
end


const θ_guess_tbbpl = (a1 = 2.4, a2 = 1.1, b = 2.1, mb = 29.0,
                       f1 = 8.6, mu1 = 9.9, sig1 = 0.81,
                       f2 = 8.2, mu2 = 30.0, sig2 = 6.8,
                       lam = 3.8, zp=2.7, kap=6.7)
