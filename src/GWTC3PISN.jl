module GWTC3PISN

using Distributions
using HDF5, MCMCChains, Cosmology, UnitfulAstro, Unitful, StatsBase, StatsFuns, Glob
using Turing, MCMCChains
using StatsPlots, KernelDensity
using SpecialFunctions
using Logging
using Printf
using Random
using LinearAlgebra
using ForwardDiff

using PISNMassFunctions

using PyCall

export li_prior_wt, read_li_posteriors
export read_sensitivity_data, draw_likelihood_samples
export corner

export θ_guess

export resample_selection

export lalsim, lal

const lalsim = PyNULL()
const lal = PyNULL()

function __init__()
    copy!(lal, pyimport_conda("lal", "lalsuite", "conda-forge"))
    copy!(lalsim, pyimport_conda("lalsimulation", "lalsuite", "conda-forge"))
end

const c = cosmology(h=0.6737, OmegaM=0.3147, OmegaK=0.0, w0=-1.0, wa=0.0)

const θ_guess = (
    a = 1.7,
    b = -0.36,
    beta = 1.7,
    mBH_max = 33.0,
    mPISN = 25.0,
    sigma = 3.4,
    f = 0.56,
    c = 2.6,
    beta_c = 1.0,
    lambda = 2.5,
    z_p = 1.8,
    kappa = 5.5
)

# In order to work with ForwardDiff, `weights` needs to know to drop the derivative terms.
function StatsBase.weights(wts::AbstractVector{ForwardDiff.Dual{T, V, N}}) where {T,N,V<:Real}
    weights([w.value for w in wts])
end

"""
    ddL_dz(z)

Returns the derivative of the luminosity distance (in Gpc) at `z`.

Not auto-diff-able due to the use of the `Cosmology` package.
"""
function ddL_dz(z)
    ustrip(u"Gpc", comoving_radial_dist(c,z) + (1+z)*Cosmology.hubble_dist0(c)/Cosmology.E(c,z))
end

"""
    li_prior_wt(m1, q, z)

The LALInference prior density at primary mass `m1`, mass ratio `q`, and
redshift `z`.

This assumes the "uniform merger rate in the comoving frame" prior (`_comoving`)
from the O3a data release.
"""
function li_prior_wt(m1, q, z)
    # LIGO prior is flat in m1_det, m2_det, and ~d_L^2
    # d(m1_det) / d(m1) = (1+z)
    # d(m2_det) / d(q) = m1*(1+z)
    # d_L^2 d(dL)/d(z)
    opz = one(z) + z
    dL = luminosity_dist(c, z)
    li_jac = opz*opz*m1*dL*dL*ddL_dz(z)
    return li_jac
end

"""
    read_li_posteriors(dir, glob_pattern="GW*[0-9].h5")

Read all HDF5 posterior sample files in `dir` of the form `glob_pattern` and
return `(samples, filenames, waveforms)`.

The reader will prioritize `PublicationSamples` and `C01:Mixed` waveforms, in
that order.

The samples are returned as an array of named tuples; filenames are a list of
strings, and waveforms are a list of strings.
"""
function read_li_posteriors(dir, glob_pattern="GW*[0-9].h5")
    all_samps = []
    all_fnames = []
    all_wfs = []
    for fn in glob(glob_pattern, dir)
        h5open(fn, "r") do f
            for waveform in ["PublicationSamples", "C01:Mixed"]
                if haskey(f, waveform)
                    push!(all_samps, read(f, waveform * "/posterior_samples"))
                    push!(all_fnames, fn)
                    push!(all_wfs, waveform)
                    break
                end
            end
        end
    end

    (all_samps, all_fnames, all_wfs)
end

"""
    read_sensitivity_data(filename; ifar_thresh=0.5)

Return `(m1, q, z, pdraw, Ndraw)` for detected injections from the
sensitivity data in `filename` with threshold above `ifar_thresh` (in years).

The draw probability is per primary mass, per mass ratio, per redshift, per year
of detector time.
"""
function read_sensitivity_data(filename; ifar_thresh=1.0)
    h5open(filename, "r") do f
        m1_sel = read(f["injections"], "mass1_source")
        m2_sel = read(f["injections"], "mass2_source")
        q_sel = m2_sel ./ m1_sel
        z_sel = read(f["injections"], "redshift")
        ifar_gstlal = read(f["injections"], "ifar_gstlal")
        ifar_pycbc_bbh = read(f["injections"], "ifar_pycbc_bbh")
        ifar_pycbc_full = read(f["injections"], "ifar_pycbc_hyperbank")
        ifar_cwb = read(f["injections"], "ifar_cwb")

        p_sel_mass = read(f["injections"], "mass1_source_mass2_source_sampling_pdf")
        p_sel_z = read(f["injections"], "redshift_sampling_pdf")
        p_sel = p_sel_mass .* p_sel_z

        T_yr = read(attributes(f), "analysis_time_s")/(365.25*24.0*3600.0)
        Ndraw = read(attributes(f), "total_generated")

        p_sel = p_sel ./ T_yr # per detector time
        p_sel = p_sel .* m1_sel # d(m2)/d(q) = m1

        sel = (ifar_gstlal .> ifar_thresh) .| (ifar_pycbc_bbh .> ifar_thresh) .| (ifar_pycbc_full .> ifar_thresh) .| (ifar_cwb .> ifar_thresh)

        (convert(Vector{Float64}, m1_sel[sel]),
         convert(Vector{Float64}, q_sel[sel]),
         convert(Vector{Float64}, z_sel[sel]),
         convert(Vector{Float64}, p_sel[sel]), Ndraw)
    end
end

"""
    draw_likelihood_samples(li_posteriors, Nsamp, wt_fn = nothing)

Return 2D arrays of size `(Nevt, Nsamp)` from the likelihood given posterior
samples.

# Arguments

- `li_posteriors::Array{Array{NamedTuple,1},1}` contains arrays of posterior
  samples represented as named tuples.

- `Nsamp::Integer` the number of samples to return from each posterior.

- `wt_fn`: optional weighting function; if given, samples will be drawn with
  this effective prior on `m_1`, `q`, and `z`.

# Return

`(m1_samples, q_samples, z_samples, wt_samples)` all of size `(Nevt, Nsamp)`.
"""
function draw_likelihood_samples(li_posteriors, Nsamp, wt_fn = nothing)
    Nobs = length(li_posteriors)

    m1samp = zeros((Nobs, Nsamp))
    qsamp = zeros((Nobs, Nsamp))
    zsamp = zeros((Nobs, Nsamp))
    wtsamp = zeros((Nobs, Nsamp))
    for (i,s) in enumerate(li_posteriors)
        m1 = [x.mass_1_source for x in s]
        m2 = [x.mass_2_source for x in s]
        q = m2 ./ m1
        z = [x.redshift for x in s]

        pwt = li_prior_wt.(m1, q, z)
        if wt_fn == nothing
            wts_numer = ones(Nsamp)
        else
            wts_numer = wt_fn.(m1, q, z)
        end
        wts = wts_numer ./ pwt
        wts /= sum(wts)
        Neff = 1.0 / sum(wts.*wts)

        if Nsamp > Neff
            @warn "Drawing more samples ($(Nsamp)) than effective samples ($(Neff))"
        end

        c = sample(collect(1:length(m1)), weights(wts), Nsamp)

        m1samp[i, :] = m1[c]
        qsamp[i,:] = q[c]
        zsamp[i,:] = z[c]
        wtsamp[i,:] = wts_numer[c]
    end

    (m1samp, qsamp, zsamp, wtsamp)
end

function has_positive_posterior_density(m, q, z)
    m2 = m*q
    (0 < q) && (q < 1) && (m2 > 5) && (z > 0)
end

include("config.jl")
export paths

include("plotting.jl")
export cornerdensity, cornerdensity!
export logscale_ticks

include("resample_selection.jl")

include("kde.jl")
export rescaled_bivariate_kde, kde_levels

include("mocks.jl")
export draw_snr, noise_snr, mchirp, eta
export draw_obs, draw_obs_approx, obs_model, snr_thresh
export simple_dl_prior_density
export draw_injections
export θ_mock, draw_population
export d_m1qz_d_m1qd_default

include("two_bump_broken_pl.jl")
export log_dNdm1dqdz_tbbpl_fn, log_dNdm1dqdz_tbbpl_paired_fn
export two_bump_broken_pl_model, two_bump_broken_pl_cosmology_model
export two_bump_broken_pl_extra_dims
export θ_guess_tbbpl
end # module
