paths = Dict(:laptop => Dict(:o3a_sampledir => "/Users/wfarr/Research/o3a_posterior_samples/all_posterior_samples",
                             :o3b_sampledir => "/Users/wfarr/Research/o3b_data/PE",
                             :sensfile => "/Users/wfarr/Research/o3b_data/O3-Sensitivity/endo3_bbhpop-LIGO-T2100113-v12.hdf5"),
             :rusty => Dict(:o3a_sampledir => "/mnt/home/wfarr/ceph/o3a_data/all_posterior_samples",
                            :o3b_sampledir => "/mnt/home/wfarr/ceph/o3b_data/PE",
                            :sensfile => "/mnt/home/wfarr/ceph/o3b_data/O3-Sensitivity/endo3_bbhpop-LIGO-T2100113-v12.hdf5"))
