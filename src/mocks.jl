import SpecialFunctions: besselix

function besselix(nu::S, x::ForwardDiff.Dual{T,V,N}) where {T, V, N, S <: Real}
   xx = ForwardDiff.value(x)
   result = besselix(nu, xx)
   ForwardDiff.Dual{T,V,N}(result, ((besselix(nu-1, xx) + besselix(nu+1, xx))/2 - sign(xx)*result) * ForwardDiff.partials(x))
end

"""
    my_ncchi2_logpdf(ν, λ, x)

The PDF for a non-central chi^2 distribution.  (This is already coded in
`Distributions`, but doesn't work with the autodiff libraries, so I
re-implemented here).

Recall that the non-central chi^2 has density

``p\\left( x \\mid \\nu, \\lambda \\right) = \\frac{1}{2} e^{-\\left( x + \\lambda\\right)/2} \\left( \\frac{x}{\\lambda} \\right)^{\\nu/4-1/2} I_{\\nu/2-1}\\left( \\sqrt{\\lambda x} \\right)``

here

``x = \\sum_{i=1}^{\\nu} x_i``

where

``x_i \\sim N\\left(\\mu_i,1\\right)``

and

``\\lambda = \\sum_{i=1}^{\\nu} \\mu_i^2``

# Parameters

- `ν`: The number of degrees of freedom.

- `λ`: The non-centrality parameter.

- `x`: The location at which to evaluate the PDF.

"""
function my_ncchi2_logpdf(ν, λ, x)
    barg = sqrt(λ*x)

    log(0.5) - (x + λ)/2 + (ν/4-1/2)*log(x/λ) + abs(barg) + log(besselix(ν/2-1, barg))
end

function next_power_of_2(x)
    y = 1.0
    while y <= x
        y *= 2
    end
    y
end

"""
    draw_snr(m1, q, dL)

Returns `[snr_H1, snr_L1, snr_V1]` the S/N for an event with the given
parameters, including random noise.

Note: `m1` is the *detector frame* mass, and `dL` should be in Gpc.  The
detector is assumed to be

Other relevant parameters (inclination, polarization, etc) will be chosen at
random.
"""
function draw_snr(m1, q, dL)
    fstart = 9.0
    flow = 10.0
    fhigh = 1024.0

    m2 = q*m1

    iota = asin(2*rand()-1)
    phi = 2*pi*rand()
    ra = 2*pi*rand()
    dec = asin(2*rand()-1)
    psi = 2*pi*rand()
    gmst = 2*pi*rand()

    T = next_power_of_2(lalsim.SimInspiralChirpTimeBound(fstart, m1*lal.MSUN_SI, m2*lal.MSUN_SI, 0.0, 0.0))
    dF = 1 / T

    hp, hc = lalsim.SimInspiralChooseFDWaveform(m1*lal.MSUN_SI, m2*lal.MSUN_SI,
                                                0.0, 0.0, 0.0,
                                                0.0, 0.0, 0.0,
                                                dL*1e9*lal.PC_SI, iota,
                                                phi, 0.0, 0.0, 0.0, dF,
                                                fstart, fhigh, fstart,
                                                nothing,
                                                lalsim.IMRPhenomPv3)

    SNRs = Float64[]
    for (dname, dtag) in zip([:H1, :L1, :V1], [lal.LHO_4K_DETECTOR, lal.LLO_4K_DETECTOR, lal.VIRGO_DETECTOR])
        d = lal.CachedDetectors[dtag]
        Fp, Fc = lal.ComputeDetAMResponse(d.response, ra, dec, psi, gmst)
        h = lal.CreateCOMPLEX16FrequencySeries("h in detector", lal.LIGOTimeGPS(), fstart, dF, hp.sampleUnits, hp.data.length)
        h.data.data = Fp .* hp.data.data .+ Fc.*hc.data.data

        psd = lal.CreateREAL8FrequencySeries("detector PSD", lal.LIGOTimeGPS(), fstart, dF, lal.DimensionlessUnit, hp.data.length)
        if dname == :V1
            lalsim.SimNoisePSDFromFile(psd, fstart, joinpath(@__DIR__, "..", "noisecurves", "avirgo_O5high_NEW.txt"))
        else
            lalsim.SimNoisePSDFromFile(psd, fstart, joinpath(@__DIR__, "..", "noisecurves", "AplusDesign.txt"))
        end

        push!(SNRs, lalsim.MeasureSNRFD(h, psd, flow, fhigh))
    end
    SNRs
end

"""
    noise_snr(snrs)

Given an array of "true" SNRs, add Gaussian noise to each one and report the
total "noisy" SNR.
"""
function noise_snr(snrs)
    sqrt(sum((snrs .+ randn(length(snrs))).^2))
end

"""
    mchirp(m1, q)

Returns the chirp mass for the given primary mass and mass ratio.
"""
function mchirp(m1, q)
    m1*q^(3/5)/(1+q)^(1/5)
end

"""
    eta(q)

Returns the symmetric mass ratio for mass ratio `q`.
"""
function eta(q)
    q/(1+q)^2
end

"""The SNR threshold used here"""
const snr_thresh = 9.0

"""
    draw_obs(m1, q, z)

Draws a synthetic observation of a binary merger with the given source-frame
primary mass, mass ratio, and redshift.

# Return

    ((snr_obs, snr_true), (A_obs, sigma_log_A, Atrue), (log_mc_obs, sigma_log_mc), (eta_obs, sigma_eta))
"""
function draw_obs(m1, q, z)
    dL = ustrip(u"Gpc", luminosity_dist(cosmology_default, z))

    m1det = m1*(1+z)
    mcdet = mchirp(m1det, q)
    et = eta(q)

    snrs = draw_snr(m1det, q, dL)
    snr_true = sqrt(sum(snrs.*snrs))

    # These numbers come from fitting a handful of sims at design sensitivity
    # with Bilby; see `scripts/fit_errors.jl`.
    x = log(snr_true / 9)
    sigma_log_mc = exp(-2.235 - 2.312*x + 0.719*randn())
    sigma_eta = exp(-1.943 - 1.189*x + 0.277*randn())
    sigma_log_dl = exp(-1.817 - 1.070*x + 0.326*randn())

    snr_obs = noise_snr(snrs)
    log_mc_obs = rand(Normal(log(mcdet), sigma_log_mc))
    eta_obs = -1.0
    while eta_obs < 0 || eta_obs > 0.25
        eta_obs = rand(Normal(et, sigma_eta))
    end
    log_dl_obs = rand(Normal(log(dL), sigma_log_dl))

    ((snr_obs, snr_true), (log_mc_obs, sigma_log_mc), (eta_obs, sigma_eta), (log_dl_obs, sigma_log_dl))
end

"""
    mu_logA

The mean of the natural log of the amplitude factor, ``A``, representing the
angluar contribution to the SNR at advanced design sensitivity for a
representative population of BBH signals:

```math
\\rho = \\frac{A \\left( M_{c,\\mathrm{detector}} / M_\\odot \\right)^{5/6}}{\\left( d_L / \\mathrm{Gpc} \\right)}
```
"""
const mu_logA = 1.23

"""
    sigma_logA

The standard deviation of the log of the amplitude factor, A.
"""
const sigma_logA = 0.425

"""
    draw_obs_approx(m1, q, z)

Analagous to `draw_obs`, but instead of generating a waveform and computing an
SNR, uses a log-normal approximation to the distribution of angular factors to
generate a random SNR.
"""
function draw_obs_approx(m1, q, z)
    m1det = m1*(1+z)
    mcdet = mchirp(m1det, q)
    et = eta(q)

    d = ustrip(u"Gpc", luminosity_dist(cosmology_default, z))

    A = exp(rand(Normal(mu_logA, sigma_logA)))

    rho = A * mcdet^(5/6) / d

    rho_obs = sqrt(rand(NoncentralChisq(3, rho*rho)))

    # These numbers come from fitting a handful of sims at design sensitivity
    # with Bilby; see `scripts/fit_errors.jl`.
    x = log(rho / 9)
    sigma_log_mc = exp(-2.235 - 2.312*x + 0.719*randn())
    sigma_eta = exp(-1.943 - 1.189*x + 0.277*randn())
    sigma_log_dl = exp(-1.817 - 1.070*x + 0.326*randn())

    log_mc_obs = rand(Normal(log(mcdet), sigma_log_mc))
    eta_obs = -1.0
    while eta_obs < 0 || eta_obs > 0.25
        eta_obs = rand(Normal(et, sigma_eta))
    end
    log_dl_obs = rand(Normal(log(d), sigma_log_dl))

    ((rho_obs, rho), (log_mc_obs, sigma_log_mc), (eta_obs, sigma_eta), (log_dl_obs, sigma_log_dl))
end

"""
    log_simple_dl_prior_density(dl)

A rational function approximation to (the log of) a
uniform-merger-rate-in-comoving-frame luminosity distance density with the
Planck15 cosmology, given by

```math
p\\left( d_L \\right) \\propto \\frac{d_L^2}{ \\sum_{i = 0}^{4} c_{i+1} d_L^i}
```

with ``\\vec{c} = \\left( 1.012306, 1.136740, 0.262462, 0.016732, 0.000387
\\right)`` and ``d_L`` in Gpc.  This approximation is good to better than 1% for
``0 \\leq z < 4``.  See https://dcc.ligo.org/LIGO-T1900192.
"""
function simple_dl_prior_density(dl)
    denom = 1.012306 + dl*(1.136740 + dl*(0.262462 + dl*(0.016732 + dl*0.000387)))

    return dl*dl / denom
end

"""
    θ_mock

A fair draw of parameters from the fit to O3a.
"""
const θ_mock = (
    a = 1.8,
    b = -0.42,
    beta = 0.67,
    c = 3.4,
    beta_c = 1.0,
    mPISN = 36.0,
    mBH_max = 38.0,
    sigma = 1.9,
    f = 0.48,
    lambda = 2.4,
    z_p = 1.5,
    kappa = 6.3,
    R = 0.18
)
_log_prior_m1qd = log_dNdtheta_cosmology_fn(θ_mock.a, θ_mock.b, θ_mock.beta, θ_mock.mPISN, θ_mock.mBH_max, θ_mock.sigma, θ_mock.f, θ_mock.c, θ_mock.beta_c, θ_mock.lambda, θ_mock.z_p, θ_mock.kappa, h_default, ΩM_default, w_default)

"""
    obs_model(snr_obs, log_mc_obs, sigma_log_mc, eta_obs, sigma_eta, log_dl_obs, sigma_log_dl)

Construct a Turing model for the observational data given.

The generated quantities from this model will be a named tuple with entries
`(:m1, :q, :d, :log_prior_wt)` giving the (detector frame) primary mass, mass
ratio, luminosity distance (Gpc), and the log of the prior weight applied to
these parameters.  The prior is the population density at parameters `θ_mock`
and the default cosmology.
"""
@model function obs_model(snr_obs, log_mc_obs, sigma_log_mc, eta_obs, sigma_eta, log_dl_obs, sigma_log_dl)
    q_obs = (1 - sqrt(abs(1 - 4 * eta_obs)) - 2 * eta_obs) / (2 * eta_obs)
    q_obs = max(q_obs, 0)
    q_obs = min(q_obs, 1)

    sigma_q_obs = min(1, sigma_eta * (1+q_obs)^3 / (1-q_obs))

    log_mc ~ Uniform(log_mc_obs - 5*sigma_log_mc, log_mc_obs + 5*sigma_log_mc)
    q ~ Uniform(max(q_obs - 5*sigma_q_obs, 0), min(q_obs + 5*sigma_q_obs, 1))
    log_dl ~ Uniform(log_dl_obs - 5*sigma_log_dl, log_dl_obs + 5*sigma_log_dl)
    snr ~ Uniform(max(snr_obs - 5*sqrt(3), 0), snr_obs + 5*sqrt(3))

    mc = exp(log_mc)
    m1 = mc / (q^(3/5) / (1+q)^(1/5))
    eta = q / (1+q)^2
    dl = exp(log_dl)
    A = snr * dl / mc^(5/6)

    log_prior_wt = _log_prior_m1qd(m1, q, dl)

    # Population prior in m1, q, d with Jacobian (m1, q, d) => (log_mc, q, log_d)
    Turing.@addlogprob! log_prior_wt + log(m1) + log_dl

    # Lognormal prior on A with Jacobian A => SNR
    Turing.@addlogprob! logpdf(LogNormal(mu_logA, sigma_logA), A) + log_dl - (5/6)*log_mc

    # Likelihoods
    log_mc_obs ~ Normal(log_mc, sigma_log_mc)
    eta_obs ~ truncated(Normal(eta, sigma_eta), 0, 1/4)
    log_dl_obs ~ Normal(log_dl, sigma_log_dl)
    Turing.@addlogprob! my_ncchi2_logpdf(3, snr*snr, snr_obs*snr_obs)

    return (m1 = m1, q = q, d = dl, log_prior_wt = log_prior_wt)
end

"""
    draw_injections(N)

A basic distribution over `m1` (source frame), `q`, `z` suitable for drawing
"injections" for detectability calculations.

Returns `(m1, q, z, pdraw)`, where `pdraw` is the density in `m1`, `q`, `z` at
the drawn point and each component is an array of successful draws.
"""
function draw_injections(N)
    mmin = 4
    mmax = 300
    zdist = truncated(Normal(3.1, 1.4), 0, Inf)
    m1s = exp.(rand(Uniform(log(mmin), log(mmax)), N))
    qs = rand(Uniform(0, 1), N)
    zs = rand(zdist, N)

    draw_wts = 1 ./ (m1s.*(log(mmax)-log(mmin))) .* pdf.(zdist, zs)

    (m1s, qs, zs, draw_wts)
end

"""
    draw_population(Ndraw=100; safety_factor=10)

Returns `(m1s, qs, zs, pdraws, norm, Neff_norm)`, draws from the population.

`norm` is an estimate of the integral of the population density over all masses
and redshifts; the expected number of merger events per year is `θ_mock.R * norm`.

`Neff_norm` is the number of effective samples in the norm computation (so the
fractional uncertainty in the normalization is `1/sqrt(Neff_norm)`).

The method used is rejection sampling; `Ndraw` draws will be taken from a simple
distribution, and then `Neff_norm / safety_factor` rejection samples (some
repeated) will be returned.
"""
function draw_population(Ndraw=10000; safety_factor = 10)
    m1s, qs, zs, draw_wt = draw_injections(Ndraw)

    log_pop_wt = log_dNdtheta_fn(θ_mock.a, θ_mock.b, θ_mock.beta, θ_mock.mPISN, θ_mock.mBH_max, θ_mock.sigma, θ_mock.f, θ_mock.c, θ_mock.lambda, θ_mock.z_p, θ_mock.kappa)

    pop_wts = exp.(log_pop_wt.(m1s, qs, zs))
    wts = pop_wts ./ draw_wt
    norm = sum(wts) / length(wts)

    Nex = sum(wts) / maximum(wts)

    Neff_norm = sum(wts)^2 / sum(wts.*wts)

    @assert Nex > 10

    inds = maximum(wts) .* rand(Ndraw) .< wts

    m1s[inds], qs[inds], zs[inds], pop_wts[inds] ./ norm, norm, Neff_norm
end

"""
    d_m1qz_d_m1qd_default(m1_source, q, z)

The Jacobian,

```math
\\frac{\\partial \\left( m_{1,\\mathrm{source}}, q, z \\right)}{\\left( m_{1,\\mathrm{detector}}, q, d_L \\right)}
```

between the source-frame mass, mass ratio, and redshift; and the detector-frame
mass, mass ratio, and luminosity distance.  We use the default (Planck 2018)
cosmology from `PISNMassFunctions`.
"""
function d_m1qz_d_m1qd_default(m1_source, q, z)
    dC = ustrip(u"Gpc", comoving_transverse_dist(cosmology_default, z))
    dL = ustrip(u"Gpc", luminosity_dist(cosmology_default, z))
    dH = ustrip(u"Gpc", hubble_dist(cosmology_default, 0))
    Ez = Cosmology.E(cosmology_default, z)

    ddLdz = dC + (1+z)*dH/Ez

    mass_J = 1 / (1 + z)
    z_J = 1 / ddLdz

    mass_J*z_J
end
