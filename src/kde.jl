"""
    rescaled_bivariate_kde((x,y))

Returns a BivariateKDE object where the density has been computed in a rotated
and scaled coordinate space with unit covariance matrix.  A better
representation of densities when the input points have non-zero correlation.
"""
function rescaled_bivariate_kde((x, y))
    z = hcat(x, y)

    mu = mean(z, dims=1)
    Sigma = cov(z, dims=1)
    L = cholesky(Sigma).L
    detL = prod(diag(L))

    zr = L \ (z .- mu)'

    kr = kde((zr[1,:], zr[2,:]))
    k = kde((x,y))
    ikr = InterpKDE(kr)

    for i in 1:size(k.x, 1)
        for j in 1:size(k.y, 1)
            zr = L \ (permutedims([k.x[i], k.y[j]]) .- mu)'
            p = pdf(ikr, zr[1,1], zr[2,1])
            k.density[i,j] = p / detL
        end
    end

    k
end

"""
    kde_levels(k, fractions)

Returns the density contour levels that (approximately) enclose the given
fractions of the probability mass.
"""
function kde_levels(k, fractions)
    kd = vec(k.density)
    kinds = sortperm(kd)
    kwts = kd .* step(k.x) .* step(k.y)
    sums = cumsum(kwts[kinds])
    levels = Float64[]
    for f in fractions
        j = searchsortedlast(sums, f*sums[end])[1]
        push!(levels, kd[kinds[j]])
    end

    levels
end

"""
    bounded_kde(xs; lower=0, upper=1)

Returns a function that evaluates a kernel density estimate assuming the domain
of `x` is bounded.
"""
function bounded_kde(xs::AbstractVector; lower=zero(xs[1]), upper=one(xs[1]))
    N = length(xs)
    bw = std(xs) / N^0.2

    function bkde(x)
        p = zero(x)
        for xx in xs
            d = Normal(xx, bw)
            p += pdf(truncated(d, lower, upper), x)
        end
        p / N
    end
    bkde
end
