These noise curves come from the [Observing Scenarios
Document](https://dcc.ligo.org/LIGO-P1200087/public); the data release lives
[here](https://dcc.ligo.org/LIGO-T2000012/public).  The `AplusDesign.txt` file
comes from the "A+ Design Target for O5"; the `avirgo_O5high_NEW.txt`
(really??!!??) comes from the "Virgo target sensitivity for O5 (high limit)".
