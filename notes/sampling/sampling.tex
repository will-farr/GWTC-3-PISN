\documentclass[modern]{aastex63}

\usepackage{amsmath}

\begin{document}

\title{Pre-Processing Injections to Improve Efficiency in Selection Function Estimates}
\author{Will M. Farr}
\affiliation{Center for Computational Astrophysics, Flatiron Institute, New York NY 10010, United States}
\affiliation{Department of Physics and Astronomy, Stony Brook University, Stony Brook NY 11794, United States}
\email{will.farr@stonybrook.edu}

\begin{abstract}
  I show how to improve the efficiency of importance-weighted Monte Carlo
  selection function estimates in population analyses through a
  computationally-trivial pre-processing step.
\end{abstract}

\section{Preliminaries and Definitions}

We follow the conventions and nomenclature of \citet{Farr2019}.  The fundamental
quantity representing selection effects in a population analysis is the
detection efficiency for a proposed population, which can be estimated using an importance-weighted Monte-Carlo by
%
\begin{equation}
  \mu\left( \lambda \right) \equiv \frac{1}{N_\mathrm{draw}} \sum_{i = 1}^{N_\mathrm{det}} \frac{\xi\left( \theta_i \mid \lambda \right)}{p_\mathrm{draw} \left( \theta_i \right)}.
\end{equation}
%
Here $\mu$ is the detection efficiency estimate for the population over
parameters $\theta$ described by the Poisson intensity $\xi$ which can depend on
``population level'' parameters $\lambda$. The sum runs over a prepared set of
$N_\mathrm{draw}$ ``injections'' whose parameters $\theta$ have been drawn from
a probability distribution $p_\mathrm{draw}$ and passed through the same
pipeline used to prepare the catalog whose population is being analyzed; only
the subset of $N_\mathrm{det}$ injections that have passed the catalog detection
threshold contribute to the sum.

The efficiency estimate has an associated uncertainty which, in the limit of
large numbers of drawn and detected injections, will follow a Gaussian
distribution with variance that is approximately
%
\begin{equation}
  \sigma^2 \equiv \frac{\mu^2}{N_\mathrm{eff}} = \frac{1}{N_\mathrm{draw}^2} \sum_{i=1}^{N_\mathrm{det}} \left( \frac{\xi\left( \theta_i \mid \lambda \right)}{p_\mathrm{draw} \left( \theta_i \right)} \right)^2 - \frac{\mu^2}{N_\mathrm{draw}}.
\end{equation}
%
We parameterize the variance by the $N_\mathrm{eff}$ parameter, which describes
the ratio of the square of the mean detection efficiency to the variance, and is
approximately equal to the number of injections drawn from $\xi$ (instead of
$p_\mathrm{draw}$) that would be required to achieve the same level of
uncertainty.  The degree of uncertainty depends on the variability of the ratio in the sum; it is minimized when the terms in the sum are constant, or $\xi \propto p_\mathrm{draw}$, when the variance becomes
%
\begin{equation}
  \label{eq:sigma2-definition}
    \sigma^2 = \mu^2 \left( \frac{1}{N_\mathrm{det}} - \frac{1}{N_\mathrm{draw}} \right),
\end{equation}
%
or
%
\begin{equation}
  \frac{1}{N_\mathrm{eff}} = \frac{1}{N_\mathrm{det}} - \frac{1}{N_\mathrm{draw}} \simeq \frac{1}{N_\mathrm{det}},
\end{equation}
%
where the final approximation assumes that there were many more injections
performed than passed the detection threshold so $N_\mathrm{det} \ll
N_\mathrm{draw}$.  If the terms in the sum are not constant, then the effective
sample number will be reduced below the number of detected injections,
$N_\mathrm{eff} < N_\mathrm{det}$, due to the mismatch between the injection
distribution and the proposed population.

\citet{Farr2019} showed that population estimation will be strongly biased
(systematic uncertainty as large or larger than statistical) when
$N_\mathrm{eff} < 4 N_\mathrm{obs}$ where $N_\mathrm{obs}$ is the catalog size;
conversely, the statistical bias from uncertainty about the catalog selection
function will be smaller than the statistical uncertainty when $N_\mathrm{eff} >
4 N_\mathrm{obs}$.

To achieve the necessary accuracy for a population analysis, many times
$N_\mathrm{obs}$ samples may be required in the sum computing $\mu$.  This sum
must be re-computed for each trial population in, for example, an MCMC over
population parameters $\lambda$ because $\mu$ is a function of the population
and its parameters $\lambda$.  If the number of samples is much larger than the
number of catalog objects, then the sensitivity estimate may dominate the
compute time for the population analysis.  To emphasize: this issue arises when
the population distributions being fit, $\xi\left( \theta \mid \lambda \right)$,
are not sufficiently similar to the distribution from which the injections have
been drawn, $p_\mathrm{draw}$, such that there is a large variance in the
importance weights within the sum that estimates $\mu$.

The identification of the source of the inefficiency immediately suggests a
solution.  If the distribution from which the injections were drawn,
$p_\mathrm{draw}$, could be modified to give a more uniform weight in the sum,
then the efficiency of the selection function estimate would increase.  It may
not be possible, though, to re-do the injection campaign; and, in any case, this
brings its own inefficiencies.  It is always possible, however, to re-sample the
existing suite of injections \emph{as if} it came from a distribution more
matched to the population.  Such resampling cannot increase the information
content of the injection suite; but it can make the computation of $\mu$ for
populations close to the re-sampled distribution much more efficient, by
focusing computational effort on the most important subset of the original
injections for determining sensitivity.  Formulas for this resampling are given
in the next section.  Importantly, such pre-processing can be performed
\emph{once}, before any given population analysis is undertaken, and therefore
is of negligible computational cost; so, the savings in efficiency can be
considerable

\section{Pre-Processing Formulas}

We wish to draw a (perhaps repeated) subset of $\tilde{N}_\mathrm{draw}$
injection parameters $\tilde{\theta}$ from a modified injection distribution
$\tilde{p}_\mathrm{draw}$ in a pre-processing step that will make our subsequent
population analysis more efficient.  (We will use a tilde to refer to quantities
in the re-sampled injection set.)  If we have some idea of the ``likely'' values
of $\lambda$ (perhaps from a preliminary MCMC over populations, even an
inefficient one, or a physically-motivated reason for choosing a ``good'' set of
population parameters), then we can choose a representative value $\lambda_0$,
and sampling efficiency for parameters close to $\lambda_0$ will be improved if
$\tilde{p}_\mathrm{draw}\left( \tilde{\theta} \right) \propto
\xi\left(\tilde{\theta} \mid \lambda_0 \right)$.

We can re-sample the existing injections as if they were drawn from the density $\tilde{p}_\mathrm{draw}$ by computing importance weights
%
\begin{equation}
  \tilde{w}_i = \frac{\xi\left( \tilde{\theta}_i \mid \lambda_0 \right)}{p_\mathrm{draw}\left( \tilde{\theta}_i \right)},
\end{equation}
%
and then constructing a sample of $\tilde{\theta}$ from the existing injection
parameters $\theta$ where each parameter is included in the sample with
probability
%
\begin{equation}
  P\left( \tilde{\theta}_i \right) = \frac{\tilde{w}_i}{\sum_j \tilde{w}_j}.
\end{equation}
%
If we repeat this sampling multiple times we will build up a set of
$\tilde{\theta}$ drawn from the desired density, but some parameters will be
duplicated multiple times.  The number of ``effectively independent'' parameters
we can draw in this way is given by
%
\begin{equation}
  \tilde{N}_\mathrm{max} = \frac{\left( \sum_i \tilde{w}_i \right)^2}{\sum_i \tilde{w}_i^2}.
\end{equation}
%
($\tilde{N}_\mathrm{max}$ represents the reduction in the variance of the mean
value of the $\tilde{w}$ compared to the variance of the $\tilde{w}$ themselves,
and is a measure of the amount of information carried by the original injection
set about the re-weighted distribution.)  We now assume that we have drawn
$\tilde{N}_\mathrm{draw} \leq \tilde{N}_\mathrm{max}$ samples $\tilde{\theta}$
according to the weights $\tilde{w}_i$.

Then the $\tilde{N}_\mathrm{draw}$ parameter samples $\tilde{\theta}$ can be
treated as if they came from a normalized draw distribution
%
\begin{equation}
  \tilde{p}_\mathrm{draw} \left( \tilde{\theta} \right) = \frac{\xi\left( \tilde{\theta} \mid \lambda_0 \right)}{\mu\left( \lambda_0 \right)},
\end{equation}
%
with
%
\begin{equation}
  \mu\left( \lambda_0 \right) = \frac{1}{N_\mathrm{draw}} \sum_{i=1}^{N_\mathrm{det}} \frac{\xi\left( \theta_i \mid \lambda_0 \right)}{p_\mathrm{draw}\left( \theta \right)} = \frac{1}{N_\mathrm{draw}} \sum_i \tilde{w}_i.
\end{equation}
%
That is, the replacement
%
\begin{eqnarray}
  \left\{ \theta_i \mid i = 1, \ldots, N_\mathrm{det} \right\} & \mapsto & \left\{ \tilde{\theta}_j \mid j = 1, \ldots, \tilde{N} \leq \tilde{N}_\mathrm{max} \right\} \\
  N_\mathrm{draw} & \mapsto & \tilde{N}_\mathrm{draw} \\
  p_\mathrm{draw} & \mapsto & \tilde{p}_\mathrm{draw}
\end{eqnarray}
%
renders the ``tilde'' variables a suitable injection set for use in population
analyses according to the formulas above \emph{as if} it had been independently
drawn from $\tilde{p}_\mathrm{draw}$ and passed through the detection pipeline.
(Note that the re-sampling runs only over detected injections, so that
$\tilde{N}_\mathrm{draw} = \tilde{N}_\mathrm{det}$, unlike in the original
injection set.)

There is some information loss from this re-weighting procedure (the re-weighted
injection set cannot contain \emph{more} information than the original), which
can be quantified.  The additional source of uncertainty beyond the standard
Monte-Carlo uncertainty in Eq.\ \eqref{eq:sigma2-definition} in the re-weighted
injection set comes from the uncertainty about the normalization
$\mu\left(\lambda_0 \right)$.  It can be represented by an effective sample
number parameter $N_\mathrm{eff,0}$ via
%
\begin{equation}
  \tilde{\sigma}^2\left( \lambda_0 \right) = \frac{\mu^2\left(\lambda_0 \right)}{\tilde{N}_\mathrm{eff,0}} = \frac{1}{N_\mathrm{draw}^2} \sum_i \tilde{w}_i^2 - \frac{\mu\left( \lambda_0 \right)^2}{N_\mathrm{draw}}.
\end{equation}
%
This uncertainty should be added in quadrature to the standard Monte-Carlo
uncertainty from Eq.\ \eqref{eq:sigma2-definition} when the re-weighted
injection set is used in a population analysis; this implies an ``effective
total sample number'' for the re-weighted injection set that is given by
%
\begin{equation}
  \frac{1}{\tilde{N}_\mathrm{eff,tot}\left(\lambda \right)} = \frac{1}{\tilde{N}_\mathrm{eff}\left( \lambda \right)} + \frac{1}{\tilde{N}_\mathrm{eff,0}},
\end{equation}
%
where $\tilde{N}_\mathrm{eff}\left(\lambda \right)$ is given by the standard
formula (Eq.\ \eqref{eq:sigma2-definition} above applied to the re-sampled,
tilde variables).

The following Python code implements the resampling algorithm above, returning a
new, re-sampled detected injection set as well as the additional uncertainty
parameter $\tilde{N}_\mathrm{eff,0}$:
\begin{verbatim}
import numpy as np
def resample_injections(xi, thetas, p_draws, Ndraw):
    wts = xi(thetas) / p_draws
    p = wts / np.sum(wts)

    # draw the maximum number of samples
    N = (np.sum(wts))**2 / np.sum(wts*wts)
    norm = np.sum(wts) / Ndraw

    thetas_new = np.random.choice(thetas, size=N, replace=True, p=p)
    p_draws_new = xi(thetas_new) / norm
    s2_new = np.sum(wts*wts)/(Ndraw*Ndraw) - norm*norm / Ndraw
    Neff_new = norm*norm / s2_new

    return (thetas_new, pdraws_new, N, Neff_new)
\end{verbatim}

\bibliography{sampling}

\end{document}
