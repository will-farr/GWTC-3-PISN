# Exploring PISN physics using the GWTC-2 O3a catalog and Turing.jl

Just playing, for now.

If you want to run the notebooks in this repo, you need to install the necessary libraries and initialize the computing environment.  First, download or install [julia](https://julialang.org/) and get it running on your machine.

Then---this is only required the first time you want to run this environment---start julia in the top-level directory of this repository and issue the following commands at the REPL:

```julia-repl
julia> ]
(@v1.5) pkg> activate .
(@v1.5) pkg> add https://git.ligo.org/will-farr/pisnmassfunctions.jl.git
(@v1.5) pkg> instantiate
(@v1.5) pkg> build
(@v1.5) pkg> <backspace>
julia> exit()
```

Once you have performed the above one-time setup tasks, you can fire up a JupyterLab to explore the notebooks in the `notebooks` directory in the right environment with the following:

```julia-repl
julia> ]
(@v1.5) pkg> activate .
(@v1.5) pkg> <backspace>
julia> using IJulia
julia> ENV["JULIA_NUM_THREADS"] = 4 # if you want multi-threaded sampling
julia> IJulia.jupyterlab(dir=pwd())
```

You can run scripts that perform various tasks (e.g. sampling the posterior over population parameters using the O3a data) from the `scripts` directory:

```bash
cd scripts
julia -t <some number of threads or 'auto'> sample.jl
```
