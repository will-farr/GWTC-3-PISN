using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using AdvancedHMC
using Cosmology
using HDF5
using Logging
using GWTC3PISN
using PISNMassFunctions
using Printf
using Turing
using Unitful
using UnitfulAstro

Nsamp = 256
Nsel = 4096
Nchain = Threads.nthreads()
Nmcmc = 1000
paired = true

location = :laptop

log_dNdm1dqdz_guess = log_dNdm1dqdz_tbbpl_fn(θ_guess_tbbpl...)

# We are going to use the function below to re-weight the selection function and
# the posterior samples; if we have it go to zero for m2 < 5, then such
# parameters will be effectively eliminated by the prior: they won't appear in
# any posterior sample nor will they appear in lists of detected injections.
dNdm1dqdz_guess = (m1,q,z) -> begin
    if q*m1 > 5
        exp(log_dNdm1dqdz_guess(m1, q, z))
    else
        zero(m1)
    end
end

(samps, fnames, wfs) = read_li_posteriors(paths[location][:sampledir])
(m1_sel, q_sel, z_sel, pdraw_sel, Ndraw) = read_sensitivity_data(paths[location][:sensfile])

bbh_mask = [quantile([xx.mass_2_source for xx in x], 0.01) > 5 for x in samps]
samps = samps[bbh_mask]
fnames = fnames[bbh_mask]
wfs = wfs[bbh_mask]

# Now we re-weight the posterior samples and the sensitivity data to dN_guess:
(m1samp, qsamp, zsamp, wtsamp) = draw_likelihood_samples(samps, Nsamp, dNdm1dqdz_guess)
log_wtsamp = log.(wtsamp)

(m1_sel, q_sel, z_sel, pdraw_sel, Ndraw, Neff_norm) = resample_selection(dNdm1dqdz_guess, m1_sel, q_sel, z_sel, pdraw_sel, Ndraw)
log_pdraw_sel = log.(pdraw_sel)

# Cut down the
N = length(m1_sel)
f = Nsel / N
m1_sel = m1_sel[1:Nsel]
q_sel = q_sel[1:Nsel]
z_sel = z_sel[1:Nsel]
log_pdraw_sel = log_pdraw_sel[1:Nsel]
Ndraw = f*Ndraw

# Translate everything to detector frame
m1detsamp = m1samp .* (1 .+ zsamp)
m1det_sel = m1_sel .* (1 .+ z_sel)

dlsamp = ustrip.((u"Gpc",), luminosity_dist.((cosmology_default,), zsamp))
dl_sel = ustrip.((u"Gpc",), luminosity_dist.((cosmology_default,), z_sel))

log_wtsamp = log_wtsamp .+ log.(d_m1qz_d_m1qd_default.(m1samp, qsamp, zsamp))
log_pdraw_sel = log_pdraw_sel .+ log.(d_m1qz_d_m1qd_default.(m1_sel, q_sel, z_sel))

model = GWTC3PISN.two_bump_broken_pl_cosmology_model(m1detsamp, qsamp, dlsamp, log_wtsamp, m1det_sel, q_sel, dl_sel, log_pdraw_sel, Ndraw; Neff_norm=Neff_norm, paired=paired)

θ0 = (
    h = 0.7,
    w = -1.0,
    ΩM = 0.3,
    alpha1 = 1.0,
    alpha2 = 2.0,
    beta = 0.0,
    mb = 45.0,
    log_f1 = log(1.0),
    log_f2 = log(1.0),
    mu1 = 10.0,
    mu2 = 35.0,
    log_sig1 = log(2.0),
    log_sig2 = log(5.0),
    lam = 2.7,
    dkap = 5.6-2.7,
    zp = 1.9
)
Turing.setchunksize(length(θ0))

sampler = Turing.NUTS(Nmcmc, 0.65; metricT = AdvancedHMC.DenseEuclideanMetric, max_depth=7)
if Nchain > 1
    trace = sample(model, sampler, MCMCThreads(), Nmcmc, Nchain, initial_theta=θ0)
else
    trace = sample(model, sampler, Nmcmc, initial_theta=θ0)
end

genq = with_logger(Logging.NullLogger()) do
    generated_quantities(model, trace)
end

full_trace = append_generated_quantities(trace, genq)

if paired
    fname = joinpath(@__DIR__, "..", "chains", "chains_tbbpl_paired_cosmology.h5")
else
    fname = joinpath(@__DIR__, "..", "chains", "chains_tbbpl_cosmology.h5")
end

h5open(fname, "w") do f
    write(f, full_trace)
end

Nobs = sum(bbh_mask)
@info @sprintf("minimum of Neff / Nobs = %.1f", minimum(full_trace[:Neff] ./ Nobs))
@info @sprintf("min Neff_samps = %.1f", minimum([minimum(full_trace[n]) for n in namesingroup(full_trace, :Neff_samps)]))
