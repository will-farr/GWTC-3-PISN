using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using HDF5
using Logging
using GWTC3PISN
using PISNMassFunctions
using Turing

Nsamp = 256
Nsel = 2048
Nchain = Threads.nthreads()
Nmcmc = 1000
paired = true

location = :laptop

# These were taken from a preliminary run.
log_dNdm1dqdz_guess = log_dNdm1dqdz_tbbpl_fn(θ_guess_tbbpl...)

# We are going to use the function below to re-weight the selection function and
# the posterior samples; if we have it go to zero for m2 < 5, then such
# parameters will be effectively eliminated by the prior: they won't appear in
# any posterior sample nor will they appear in lists of detected injections.
dNdm1dqdz_guess = (m1,q,z) -> begin
    if q*m1 > 5
        exp(log_dNdm1dqdz_guess(m1, q, z))
    else
        zero(m1)
    end
end

(samps, fnames, wfs) = read_li_posteriors(paths[location][:sampledir])
(m1_sel, q_sel, z_sel, pdraw_sel, Ndraw) = read_sensitivity_data(paths[location][:sensfile])

bbh_mask = [quantile([xx.mass_2_source for xx in x], 0.01) > 3 for x in samps]
samps = samps[bbh_mask]
fnames = fnames[bbh_mask]
wfs = wfs[bbh_mask]

# Now we re-weight the posterior samples and the sensitivity data to dN_guess:
(m1samp, qsamp, zsamp, wtsamp) = draw_likelihood_samples(samps, Nsamp, dNdm1dqdz_guess)
log_wtsamp = log.(wtsamp)

(m1_sel, q_sel, z_sel, pdraw_sel, Ndraw, Neff_norm) = resample_selection(dNdm1dqdz_guess, m1_sel, q_sel, z_sel, pdraw_sel, Ndraw)
log_pdraw_sel = log.(pdraw_sel)

# Cut down the
N = length(m1_sel)
f = Nsel / N
m1_sel = m1_sel[1:Nsel]
q_sel = q_sel[1:Nsel]
z_sel = z_sel[1:Nsel]
log_pdraw_sel = log_pdraw_sel[1:Nsel]
Ndraw = f*Ndraw

model = GWTC3PISN.two_bump_broken_pl_model(m1samp, qsamp, zsamp, log_wtsamp, m1_sel, q_sel, z_sel, log_pdraw_sel, Ndraw; Neff_norm=Neff_norm, paired=paired)

if Nchain > 1
    trace = sample(model, NUTS(), MCMCThreads(), Nmcmc, Nchain)
else
    trace = sample(model, NUTS(), Nmcmc)
end

genq = with_logger(Logging.NullLogger()) do
    generated_quantities(model, trace)
end

full_trace = append_generated_quantities(trace, genq)

if paired
    fname = joinpath(@__DIR__, "..", "chains", "chains_tbbpl_paired.h5")
else
    fname = joinpath(@__DIR__, "..", "chains", "chains_tbbpl.h5")
end

h5open(fname, "w") do f
    write(f, full_trace)
end
