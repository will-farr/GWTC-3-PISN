using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using AdvancedHMC
using Cosmology
using HDF5
using Logging
using GWTC3PISN
using PISNMassFunctions
using StatsBase
using StatsFuns
using Turing
using Unitful
using UnitfulAstro

Nevt = 128
Nsamp = 16
Niter = 1000
Nthread = Threads.nthreads()
Nsel_factor = 4

# Sampler params
δ = 0.65 # Acceptance ratio
max_depth = 7 # Tree depth
init_ϵ = 0.2

# Weight function
log_wt_fn = log_dNdtheta_cosmology_fn(
    θ_mock.a, θ_mock.b, θ_mock.beta,
    θ_mock.mPISN, θ_mock.mBH_max, θ_mock.sigma,
    θ_mock.f, θ_mock.c,
    θ_mock.lambda, θ_mock.z_p, θ_mock.kappa,
    h_default, ΩM_default, w_default
)

# Load the samples and re-weight
m1det_samples, q_samples, dl_samples, logwt_samples, neffs = h5open(joinpath(@__DIR__, "..", "mocks", "observations.h5"), "r") do f
    msamp = read(f, "likelihood/m1_detector")
    qsamp = read(f, "likelihood/q")
    dsamp = read(f, "likelihood/dl")
    lwsamp = read(f, "likelihood/logwt")

    Nobs = size(msamp,2)

    m1d = zeros(Nobs, Nsamp)
    q = zeros(Nobs, Nsamp)
    d = zeros(Nobs, Nsamp)

    logwt = zeros(Nobs, Nsamp)

    neffs = zeros(Nobs)

    for i in 1:Nobs
        lw = log_wt_fn.(msamp[:,i], qsamp[:,i], dsamp[:,i]) .- lwsamp[:,i]
        w = exp.(lw .- logsumexp(lw))

        N = sum(w)^2/sum(w.*w)

        if N < Nsamp
            @warn "Trying to draw more samples ($(Nsamp)) than effective samples ($(N)) for event $(i)"
        end

        neffs[i] = N

        inds = sample(1:size(msamp,1), weights(w), Nsamp)
        m1d[i,:] = msamp[inds,i]
        q[i,:] = qsamp[inds,i]
        d[i,:] = dsamp[inds,i]
        logwt[i,:] = lw[inds]
    end

    (m1d, q, d, logwt, neffs)
end

Nevt = min(Nevt, size(m1det_samples, 1))  # Just in case we ask for the ridiculous above
T = Nevt / size(m1det_samples, 1) # We only observe for this fraction of a year.

m1det_samples = m1det_samples[1:Nevt, :]
q_samples = q_samples[1:Nevt, :]
dl_samples = dl_samples[1:Nevt, :]
logwt_samples = logwt_samples[1:Nevt, :]

# Load the selection function
m1det_sel, q_sel, dl_sel, logp_sel, Ndraw = h5open(joinpath(@__DIR__, "..", "mocks", "selection.h5"), "r") do f
    m1d = read(f, "m1_detector")
    logp_sel = log.(read(f, "pdf") ./ T)
    qs = read(f, "q")
    dls = read(f, "luminosity_distance")

    Nd = read(attributes(f), "Ndraw")

    (m1d, qs, dls, logp_sel, Nd)
end

# We want to re-weight to our population
pop_wts = exp.(log_wt_fn.(m1det_sel, q_sel, dl_sel))
wts = pop_wts ./ exp.(logp_sel)
norm = sum(wts) / Ndraw
Neff_norm = sum(wts)^2/sum(wts.*wts)

@info "Selection function: found Neff_norm = $(round(Int, Neff_norm)); norm = $(norm)"
@info "Would predict $(norm * θ_mock.R) detections from mock parameters."

N = size(m1det_samples, 1)
Nsel = Nsel_factor * N
inds = sample(1:length(m1det_sel), weights(wts), Nsel)
m1det_sel = m1det_sel[inds]
q_sel = q_sel[inds]
dl_sel = dl_sel[inds]
logp_sel = log.(pop_wts[inds] ./ norm)
Ndraw = Nsel

model = pop_model_cosmology(
    m1det_samples, q_samples, dl_samples, logwt_samples,
    m1det_sel, q_sel, dl_sel, logp_sel, Ndraw;
    Neff_norm = Neff_norm
)

x = h_default^2*ΩM_default
y = h_default^2*(1-ΩM_default)

mp_min = 0.95*20
mp_max = 0.95*θ_mock.mBH_max
f_mPISN = (θ_mock.mPISN - mp_min) / (mp_max - mp_min)

θ0 = [
    θ_mock.a, θ_mock.b, θ_mock.c,
    θ_mock.mBH_max, f_mPISN, θ_mock.sigma, θ_mock.f,
    θ_mock.beta,
    θ_mock.lambda, θ_mock.z_p, θ_mock.kappa-θ_mock.lambda,
    x, y, w_default
]

Turing.setchunksize(length(θ0))
sampler = Turing.NUTS(Niter, δ; metricT = AdvancedHMC.DenseEuclideanMetric, max_depth=max_depth, init_ϵ=init_ϵ)
if Nthread > 1
    trace = sample(model, sampler, MCMCThreads(), Niter, Nthread, init_theta = θ0, progress=true)
else
    trace = sample(model, sampler, Niter, init_theta = θ0, progress=true)
end

# Sometimes warnings crop up here
trace = with_logger(NullLogger()) do
    append_generated_quantities(trace, generated_quantities(model, trace))
end

h5open(joinpath(@__DIR__, "..", "mocks", "chain.h5"), "w") do f
    write(f, trace)
end

@info "minimum of Neff / Nobs = $(minimum(trace[:Neff])/size(m1det_samples,1))"
@info "minimum of Neff_samps $(minimum([minimum(trace[n]) for n in namesingroup(trace, :Neff_samps)]))"
