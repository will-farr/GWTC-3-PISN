using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using Cosmology
using Distributions
using HDF5
using LinearAlgebra
using Logging
using GWTC3PISN
using PISNMassFunctions
using Printf
using StatsBase
using StatsFuns
using Statistics
using Turing
using Unitful
using UnitfulAstro

if isdefined(Main, :Juno)
    import ProgressLogging: @withprogress, @logprogress
else
    using ProgressLogging
    using TerminalLoggers

    global_logger(TerminalLogger())
end

T = 1.0

(m1s, qs, zs, observations) = with_seed(9042658431105968850) do
    (m1s, qs, zs) = @withprogress name="Drawing Population" begin
        m1s, qs, zs, pdraws, norm, Neff_norm = draw_population(1000000)
        Nex = T * norm * θ_mock.R
        Ndraw = rand(Poisson(Nex))

        @logprogress length(m1s) / Ndraw

        while length(m1s) < Ndraw
            m,q,z,p,n,Ne = draw_population(1000000)

            new_norm = (Neff_norm*norm + Ne*n)/(Neff_norm + Ne)
            New_neff = Neff_norm + Ne

            m1s = vcat(m1s, m)
            qs = vcat(qs, q)
            zs = vcat(zs, z)
            pdraws = vcat(pdraws.*norm./new_norm, p.*n./new_norm)
            norm = new_norm
            Neff = New_neff

            Nex = T * norm * θ_mock.R
            Ndraw = rand(Poisson(Nex))

            @logprogress length(m1s) / Ndraw
        end

        m1s = m1s[1:Ndraw]
        qs = qs[1:Ndraw]
        zs = zs[1:Ndraw]

        (m1s, qs, zs)
    end

    @progress name="Drawing observations" observations = [draw_obs_approx(m1, q, z) for (m1,q,z) in zip(m1s, qs, zs)]
    (m1s, qs, zs, observations)
end

detected_mask = [o[1][1] > GWTC3PISN.snr_thresh for o in observations]
@info "Detected $(sum(detected_mask)) events over $(T) years."

m1_samples = Array{Float64,1}[]
q_samples = Array{Float64, 1}[]
dl_samples = Array{Float64, 1}[]
logwt_samples = Array{Float64,1}[]
with_seed(8623440952208035927) do
    @progress name="Parameter estimation" for ((snr_obs, snr_true), (log_mc_obs, sigma_log_mc), (eta_obs, sigma_eta), (log_dl_obs, sigma_log_dl)) in observations[detected_mask]
        model = GWTC3PISN.obs_model(snr_obs, log_mc_obs, sigma_log_mc, eta_obs, sigma_eta, log_dl_obs, sigma_log_dl)

        thin = 4

        (trace, genq) = with_logger(NullLogger()) do
            trace = sample(model, NUTS(), thin*1000, progress=false)
            genq = generated_quantities(model, trace)
            (trace, genq)
        end

        m1 = vec([x.m1 for x in genq])
        q = vec([x.q for x in genq])
        d = vec([x.d for x in genq])
        logwt = vec([x.log_prior_wt for x in genq])

        push!(m1_samples, m1[1:thin:end])
        push!(q_samples, q[1:thin:end])
        push!(dl_samples, d[1:thin:end])
        push!(logwt_samples, logwt[1:thin:end])
    end
end

m1_samples = hcat(m1_samples...)
q_samples = hcat(q_samples...)
dl_samples = hcat(dl_samples...)
logwt_samples = hcat(logwt_samples...)

chunksize=size(m1_samples,1)

h5open(joinpath(@__DIR__, "..", "mocks", "observations.h5"), "w") do f
    t = create_group(f, "truth")
    s = create_group(f, "likelihood")
    o = create_group(f, "observed")

    t["m1_source", compress=3, shuffle=()] = m1s[detected_mask]
    t["q", compress=3, shuffle=()] = qs[detected_mask]
    t["z", compress=3, shuffle=()] = zs[detected_mask]

    s["m1_detector", chunk=(chunksize,1), compress=3, shuffle=()] = m1_samples
    s["q", chunk=(chunksize, 1), compress=3, shuffle=()] = q_samples
    s["dl", chunk=(chunksize,1), compress=3, shuffle=()] = dl_samples
    s["logwt", chunk=(chunksize,1), compress=3, shuffle=()] = logwt_samples

    obs = observations[detected_mask]
    o["snr_obs", compress=3, shuffle=()] = [o[1][1] for o in obs]
    o["snr_true", compress=3, shuffle=()] = [o[1][2] for o in obs]
    o["log_mc_obs", compress=3, shuffle=()] = [o[2][1] for o in obs]
    o["sigma_log_mc", compress=3, shuffle=()] = [o[2][2] for o in obs]
    o["eta_obs", compress=3, shuffle=()] = [o[3][1] for o in obs]
    o["sigma_eta", compress=3, shuffle=()] = [o[3][2] for o in obs]
    o["log_dl_obs", compress=3, shuffle=()] = [o[4][1] for o in obs]
    o["sigma_log_dl", compress=3, shuffle=()] = [o[4][2] for o in obs]
end

m1_sel, q_sel, dl_sel, p_sel, Ndraw = with_seed(4094348737657322525) do
    N = sum(detected_mask)
    Ndesired = 1000*N

    m1_draw = Float64[]
    q_draw = Float64[]
    d_draw = Float64[]
    z_draw = Float64[]
    m1_source_draw = Float64[]
    draw_wt = Float64[]

    Ndraw = 0

    @withprogress name="Selection Function" begin
        while length(m1_draw) < Ndesired
            # This odd shape is because draw_injections returns arrays
            (m1_source,), (q,), (z,), (pdraw,) = draw_injections(1)
            m1 = m1_source * (1 + z)

            pdraw = pdraw * d_m1qz_d_m1qd_default(m1_source, q, z)
            d = ustrip(u"Gpc", luminosity_dist(cosmology_default, z))

            obs = draw_obs_approx(m1_source, q, z)
            Ndraw += 1

            if obs[1][1] > snr_thresh
                push!(m1_draw, m1)
                push!(q_draw, q)
                push!(z_draw, z)
                push!(d_draw, d)
                push!(m1_source_draw, m1_source)
                push!(draw_wt, pdraw)

                @logprogress length(m1_draw) / Ndesired
            end
        end
    end

    (m1_draw, q_draw, d_draw, draw_wt, Ndraw)
end

dNdm1ddqdd = begin
    args = [getindex(θ_mock, s) for s in [:a, :b, :beta, :mPISN, :mBH_max, :sigma, :f, :c, :lambda, :z_p, :kappa]]

    log_dN = log_dNdtheta_fn(args...)

    zs = expm1.(log(1.0):0.01:log(1.0+10.0))
    ds = [ustrip(u"Gpc", luminosity_dist(cosmology_default, z)) for z in zs]

    z_of_d = interp(ds, zs)

    (m1d, q, dl) -> begin
        z = z_of_d(dl)
        m1 = m1d/(1+z)
        θ_mock.R * exp(log_dN(m1, q, z)) * d_m1qz_d_m1qd_default(m1, q, z)
    end
end

wts = dNdm1ddqdd.(m1_sel, q_sel, dl_sel) ./ (p_sel ./ T)
@info @sprintf("Selection function would expect %.1f detections", sum(wts) / Ndraw)

h5open(joinpath(@__DIR__, "..", "mocks", "selection.h5"), "w") do f
    f["m1_detector", compress=3, shuffle=()] = m1_sel
    f["q", compress=3, shuffle=()] = q_sel
    f["luminosity_distance", compress=3, shuffle=()] = dl_sel
    f["pdf", compress=3, shuffle=()] = p_sel
    attributes(f)["Ndraw"] = Ndraw
end
