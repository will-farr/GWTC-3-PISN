using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using DataFrames
using DelimitedFiles
using LaTeXStrings
using GWTC3PISN
using Printf
using StatsBase
using Statistics
using StatsPlots

data, header = readdlm(joinpath(@__DIR__, "..", "bilby", "snr_uncertainty.csv"), ',', header=true)
df = DataFrame(data, [Symbol(s) for s in vec(header)])

fmtr = x -> @sprintf("%g", x)

M = hcat(ones(size(df,1)), log.(df[:, :SNR] ./ 9))

c_mc = M \ log.(df[:, :sigma_log_mc])
c_eta = M \ log.(df[:, :sigma_eta])
c_dl = M \ log.(df[:, :sigma_log_dl])

r_mc = log.(df[:, :sigma_log_mc]) .- M * c_mc
r_eta = log.(df[:, :sigma_eta]) .- M * c_eta
r_dl = log.(df[:, :sigma_log_dl]) .- M * c_dl

rss_mc = sqrt(mean(r_mc .* r_mc))
rss_eta = sqrt(mean(r_eta .* r_eta))
rss_dl = sqrt(mean(r_dl .* r_dl))

x = exp.(log(minimum(df[:,:SNR])):0.01:log(maximum(df[:,:SNR])))

@df df scatter(:SNR, :sigma_log_mc,
               xlabel=L"\rho", ylabel=L"\sigma_{\log M_c}",
               xscale=:log10, xticks=logscale_ticks(:SNR),
               yscale=:log10, yticks=logscale_ticks(:sigma_log_mc),
               legend=nothing, formatter=fmtr)
plot!(x, exp.(c_mc[1] .+ c_mc[2].*log.(x./9)), color=:black)

@df df scatter(:SNR, :sigma_eta,
              xlabel=L"\rho", ylabel=L"\sigma_{\eta}",
              xscale=:log10, xticks=logscale_ticks(:SNR),
              yscale=:log10, yticks=logscale_ticks(:sigma_eta),
              legend=nothing, formatter=fmtr)
plot!(x, exp.(c_eta[1] .+ c_eta[2].*log.(x./9)), color=:black)

@df df scatter(:SNR, :sigma_log_dl,
               xlabel=L"\rho", ylabel=L"\sigma_{\log d_L}",
               xscale=:log10, xticks=logscale_ticks(:SNR),
               yscale=:log10, yticks=logscale_ticks(:sigma_log_dl),
               legend=nothing, formatter=fmtr)
plot!(x, exp.(c_dl[1] .+ c_dl[2].*log.(x./9)), color=:black)
