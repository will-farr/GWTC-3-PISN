using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using HDF5
using Logging
using MCMCChains
using GWTC3PISN
using PISNMassFunctions
using Random
using StatsBase
using Statistics
using Turing

location = :rusty

Nlike = 128
Nsel = 2048
Nsamp = 1000
Nchain = Threads.nthreads()

samps, fnames, wfs = GWTC3PISN.read_li_posteriors(paths[location][:sampledir])
bbh_samps = [s for s in samps if quantile([x.mass_2_source for x in s], 0.01) > 5]
bbh_fnames = [f for (s, f) in zip(samps, fnames) if quantile([x.mass_2_source for x in s], 0.01) > 5]

θ0, wt_fn =
    let a = 1.7, b = -0.24, beta = 1.8, mBH_max = 34.0, mPISN = 25.0, sigma = 3.7,
        f = 0.54, c = 2.6, lambda = 2.4, z_p = 1.8, kappa = 5.4,
        mPISN_min = 0.95*20, mPISN_max = 0.95*mBH_max, f_mPISN = (mPISN - mPISN_min)/(mPISN_max - mPISN_min)
    θ0 = [a, b, c, mBH_max, f_mPISN, sigma, f, beta, lambda, z_p, kappa-lambda]
    Turing.setchunksize(length(θ0))
    f = PISNMassFunctions.log_dNdtheta_fn(a, b, beta, mPISN, mBH_max, sigma, f, c, lambda, z_p, kappa)
    wt_fn = (m1,q,z) -> exp(f(m1,q,z))
    (θ0, wt_fn)
end

m1ls, qls, zls, wts = with_seed(9187647997346968997) do
    GWTC3PISN.draw_likelihood_samples(bbh_samps, Nlike, wt_fn)
end

m1sel, qsel, zsel, psel, Ndraw = GWTC3PISN.read_sensitivity_data(paths[location][:sensfile])
s = GWTC3PISN.has_positive_posterior_density.(m1sel, qsel, zsel)
m1sel = m1sel[s]
qsel = qsel[s]
zsel = zsel[s]
psel = psel[s]

m1sel, qsel, zsel, psel, Ndraw, Neff_norm = with_seed(5098374755369260857) do
    GWTC3PISN.resample_selection(wt_fn, m1sel, qsel, zsel, psel, Ndraw)
end

m1sel_cut = m1sel[1:Nsel]
qsel_cut = qsel[1:Nsel]
zsel_cut = zsel[1:Nsel]
psel_cut = psel[1:Nsel]
Ndraw_cut = round(Int, Ndraw * Nsel / length(m1sel))

m = PISNMassFunctions.pop_model(m1ls, qls, zls, log.(wts), m1sel_cut, qsel_cut, zsel_cut, log.(psel_cut), Ndraw_cut; Neff_norm=Neff_norm)

if Nchain > 1
    trace = sample(m, NUTS(Nsamp, 0.65, max_depth=7), MCMCThreads(), Nsamp, Nchain, initial_theta = θ0)
else
    trace = sample(m, NUTS(Nsamp, 0.65, max_depth=7), Nsamp, initial_theta = θ0)
end
# Sometimes warnings crop up here
trace = with_logger(NullLogger()) do
    append_generated_quantities(trace, generated_quantities(m, trace))
end

h5open(joinpath(@__DIR__, "..", "chains", "chains.h5"), "w") do f
    write(f, trace)
end

open(joinpath(@__DIR__, "..", "chains", "bbh_fnames.txt"), "w") do f
    for n in bbh_fnames
        write(f, n)
        write(f, "\n")
    end
end

Neff_01 = quantile(vec(trace[:Neff]), 0.01)
Neff_samps_01 = [quantile(vec(trace[n]), 0.01) for n in namesingroup(trace, :Neff_samps)]

@info "1% Neff for selection function = $(Neff_01)"
@info "5 x Nobs = $(5*size(m1ls,1))"
@info "minimum 1% Neff of event samples = $(minimum(Neff_samps_01))"
