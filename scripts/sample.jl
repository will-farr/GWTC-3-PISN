using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using HDF5
using Logging
using MCMCChains
using MCMCChainsStorage
using GWTC3PISN
using PISNMassFunctions
using Random
using StatsBase
using Statistics
using Turing

loc = :laptop

Nlike = 128
Nsel = 4096
Nsamp = 1000
Nchain = Threads.nthreads()

samps = []
fnames = []
wfs = []

x,y,z = GWTC3PISN.read_li_posteriors(paths[loc][:o3a_sampledir], "GW*[0-9].h5")
samps = append!(samps, x)
fnames = append!(fnames,y)
wfs = append!(wfs,z)

x,y,z = GWTC3PISN.read_li_posteriors(paths[loc][:o3b_sampledir], "*GW[0-9]*_nocosmo.h5")
samps = append!(samps, x)
fnames = append!(fnames, y)
wfs = append!(wfs, z)

bbh_mask = [quantile([x.mass_2_source for x in s], 0.01) > 5 for s in samps]
bbh_samps = samps[bbh_mask]
bbh_fnames = fnames[bbh_mask]
bbh_wfs = fnames[bbh_mask]

mPISN_min = 0.95*20
mPISN_max = 0.95*θ_guess.mBH_max
f_mPISN = (θ_guess.mPISN - mPISN_min) / (mPISN_max - mPISN_min)
θ0 = [θ_guess.a, θ_guess.b, θ_guess.c, θ_guess.mBH_max, f_mPISN, θ_guess.sigma,
      θ_guess.f, θ_guess.beta, θ_guess.lambda, θ_guess.z_p, θ_guess.kappa - θ_guess.lambda]
log_wt_fn = PISNMassFunctions.log_dNdtheta_fn(θ_guess.a, θ_guess.b, θ_guess.beta, θ_guess.mPISN, θ_guess.mBH_max, θ_guess.sigma, θ_guess.f, θ_guess.c, θ_guess.beta_c, θ_guess.lambda, θ_guess.z_p, θ_guess.kappa)
wt_fn = (m,q,z) -> exp(log_wt_fn(m,q,z))

m1ls, qls, zls, wts = with_seed(5498040261824875110) do
    GWTC3PISN.draw_likelihood_samples(bbh_samps, Nlike, wt_fn)
end

m1sel, qsel, zsel, psel, Ndraw = GWTC3PISN.read_sensitivity_data(paths[loc][:sensfile])
s = GWTC3PISN.has_positive_posterior_density.(m1sel, qsel, zsel)
m1sel = m1sel[s]
qsel = qsel[s]
zsel = zsel[s]
psel = psel[s]

m1sel, qsel, zsel, psel, Ndraw, Neff_norm = with_seed(5587913346742583518) do
    GWTC3PISN.resample_selection(wt_fn, m1sel, qsel, zsel, psel, Ndraw)
end

m1sel_cut = m1sel[1:Nsel]
qsel_cut = qsel[1:Nsel]
zsel_cut = zsel[1:Nsel]
psel_cut = psel[1:Nsel]
Ndraw_cut = round(Int, Ndraw * Nsel / length(m1sel))

m = PISNMassFunctions.pop_model(m1ls, qls, zls, log.(wts), m1sel_cut, qsel_cut, zsel_cut, log.(psel_cut), Ndraw_cut; Neff_norm=Neff_norm)

if Nchain > 1
    trace = sample(m, NUTS(Nsamp, 0.65, max_depth=7), MCMCThreads(), Nsamp, Nchain, initial_theta = θ0)
else
    trace = sample(m, NUTS(Nsamp, 0.65, max_depth=7), Nsamp, initial_theta = θ0)
end
# Sometimes warnings crop up here
trace = with_logger(NullLogger()) do
    append_generated_quantities(trace, generated_quantities(m, trace))
end

h5open(joinpath(@__DIR__, "..", "chains", "chains.h5"), "w") do f
    write(f, trace)
end

open(joinpath(@__DIR__, "..", "chains", "bbh_fnames.txt"), "w") do f
    for n in bbh_fnames
        write(f, n)
        write(f, "\n")
    end
end

Neff = minimum(trace[:Neff])
Neff_samps = minimum([minimum(trace[k]) for k in namesingroup(trace, :Neff_samps)])

@info "minimum Neff for selection function = $(Neff)"
@info "5 x Nobs = $(5*size(m1ls,1))"
@info "minimum Neff of event samples = $(Neff_samps)"
