using Pkg
Pkg.activate(joinpath(@__DIR__, ".."))

using AdvancedHMC
using Cosmology
using HDF5
using Logging
using MCMCChains
using GWTC3PISN
using PISNMassFunctions
using Random
using StatsBase
using Statistics
using Turing
using Unitful
using UnitfulAstro

loc = :rusty

Nlike = 256
Nsel = 2048
Nsamp = 1000
Nchain = Threads.nthreads()

sigma_w_small = 1e-6
sigma_w_large = 0.2

# Fitting LCDM
sigma_w = sigma_w_small

samps = []
fnames = []
wfs = []

x,y,z = GWTC3PISN.read_li_posteriors(paths[loc][:o3a_sampledir], "GW*[0-9].h5")
samps = append!(samps, x)
fnames = append!(fnames,y)
wfs = append!(wfs,z)

x,y,z = GWTC3PISN.read_li_posteriors(paths[loc][:o3b_sampledir], "*GW[0-9]*_nocosmo.h5")
samps = append!(samps, x)
fnames = append!(fnames, y)
wfs = append!(wfs, z)

bbh_mask = [quantile([x.mass_2_source for x in s], 0.01) > 5 for s in samps]
bbh_samps = samps[bbh_mask]
bbh_fnames = fnames[bbh_mask]
bbh_wfs = fnames[bbh_mask]

mPISN_min = 0.95*20
mPISN_max = 0.95*θ_guess.mBH_max
f_mPISN = (θ_guess.mPISN - mPISN_min) / (mPISN_max - mPISN_min)
θ0 = [θ_guess.a, θ_guess.b, θ_guess.c, θ_guess.mBH_max, f_mPISN, θ_guess.sigma,
      θ_guess.f, θ_guess.beta, θ_guess.lambda, θ_guess.z_p, θ_guess.kappa - θ_guess.lambda]

θ0_cosmo = vcat(θ0, [ΩM_default*h_default^2, (1-ΩM_default)*h_default^2, -1 - w_default])

log_wt_fn = PISNMassFunctions.log_dNdtheta_fn(θ_guess.a, θ_guess.b, θ_guess.beta, θ_guess.mPISN, θ_guess.mBH_max, θ_guess.sigma, θ_guess.f, θ_guess.c, θ_guess.lambda, θ_guess.z_p, θ_guess.kappa)
wt_fn = (m,q,z) -> exp(log_wt_fn(m,q,z))

m1ls, qls, zls, wts = with_seed(9187647997346968997) do
    GWTC3PISN.draw_likelihood_samples(bbh_samps, Nlike, wt_fn)
end

m1sel, qsel, zsel, psel, Ndraw = GWTC3PISN.read_sensitivity_data(paths[loc][:sensfile])
s = GWTC3PISN.has_positive_posterior_density.(m1sel, qsel, zsel)
m1sel = m1sel[s]
qsel = qsel[s]
zsel = zsel[s]
psel = psel[s]

m1sel, qsel, zsel, psel, Ndraw, Neff_norm = with_seed(5098374755369260857) do
    GWTC3PISN.resample_selection(wt_fn, m1sel, qsel, zsel, psel, Ndraw)
end

m1sel_cut = m1sel[1:Nsel]
qsel_cut = qsel[1:Nsel]
zsel_cut = zsel[1:Nsel]
psel_cut = psel[1:Nsel]
Ndraw_cut = round(Int, Ndraw * Nsel / length(m1sel))

# Now transform everything into detector-frame, luminosity distance, etc
dls = ustrip.((u"Gpc",), luminosity_dist.((GWTC3PISN.c,), zls))
wts = wts .* d_m1qz_d_m1qd_default.(m1ls, qls, zls)
m1ls = m1ls .* (1 .+ zls)

dlsel_cut = ustrip.((u"Gpc",), luminosity_dist.((GWTC3PISN.c,), zsel_cut))
psel_cut = psel_cut .* d_m1qz_d_m1qd_default.(m1sel_cut, qsel_cut, zsel_cut)
m1sel_cut = m1sel_cut .* (1 .+ zsel_cut)

m = PISNMassFunctions.pop_model_cosmology(m1ls, qls, dls, log.(wts), m1sel_cut, qsel_cut, dlsel_cut, log.(psel_cut), Ndraw_cut; Neff_norm=Neff_norm, sigma_w=sigma_w)
sampler = Turing.NUTS() # Nsamp, 0.65; metricT = AdvancedHMC.DenseEuclideanMetric, max_depth=7)
if Nchain > 1
    trace = sample(m, sampler, MCMCThreads(), Nsamp, Nchain, initial_theta = θ0_cosmo)
else
    trace = sample(m, sampler, Nsamp, initial_theta = θ0_cosmo)
end
# Sometimes warnings crop up here
trace = with_logger(NullLogger()) do
    append_generated_quantities(trace, generated_quantities(m, trace))
end

if sigma_w == sigma_w_large
    fname = joinpath(@__DIR__, "..", "chains", "chains_cosmology_wCDM.h5")
    bfname = joinpath(@__DIR__, "..", "chains", "bbh_fnames_cosmology_wCDM.txt")
else
    if sigma_w != sigma_w_small
        @warn "sigma_w = $(sigma_w), which is not the pre-set sigma_w_small = $(sigma_w_small)"
    end
    fname = joinpath(@__DIR__, "..", "chains", "chains_cosmology.h5")
    bfname = joinpath(@__DIR__, "..", "chains", "bbh_fnames_cosmology.txt")
end


h5open(fname, "w") do f
    write(f, trace)
end

open(bfname, "w") do f
    for n in bbh_fnames
        write(f, n)
        write(f, "\n")
    end
end

Neff_min = minimum(trace[:Neff])
Neff_samps_min = [minimum(trace[n]) for n in namesingroup(trace, :Neff_samps)]

@info "minimum Neff for selection function = $(Neff_min)"
@info "5 x Nobs = $(5*size(m1ls,1))"
@info "minimum Neff of event samples = $(minimum(Neff_samps_min))"
